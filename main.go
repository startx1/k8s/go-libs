package main

import (
	"fmt"
	"os"

	sxConfluence "gitlab.com/startx1/k8s/go-libs/pkg/confluence"
	sxGDrive "gitlab.com/startx1/k8s/go-libs/pkg/gdrive"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

func main() {
	display := sxUtils.NewCmdDisplay("main")
	display.Debug("Start the main go-lib function")
	args := sxUtils.NewArgParser(os.Args)
	if args.IsHelp {
		sxUtils.DisplayHelpCmd()
	}
	switch args.TopAction {
	case "version":
		sxUtils.DisplayVersion()
	case "test-gdrive":
		gdrive := sxGDrive.NewGDrive(
			os.Getenv("GDRIVE_CREDENTIAL_FILENAME"),
			os.Getenv("GDRIVE_CREDENTIAL_PATH"),
			os.Getenv("GDRIVE_CREDENTIAL_DOMAIN"),
		)
		err := gdrive.SetCredentialsContent(
			os.Getenv("GDRIVE_CREDS_TYPE"),
			os.Getenv("GDRIVE_CREDS_PROJECT_ID"),
			os.Getenv("GDRIVE_CREDS_PRIVATE_KEY_ID"),
			os.Getenv("GDRIVE_CREDS_PRIVATE_KEY"),
			os.Getenv("GDRIVE_CREDS_CLIENT_EMAIL"),
			os.Getenv("GDRIVE_CREDS_CLIENT_ID"),
			os.Getenv("GDRIVE_CREDS_AUTH_URI"),
			os.Getenv("GDRIVE_CREDS_TOKEN_URI"),
			os.Getenv("GDRIVE_CREDS_AUTH_PROVIDER_X509_CERT_URL"),
			os.Getenv("GDRIVE_CREDS_CLIENT_X509_CERT_URL"),
			os.Getenv("GDRIVE_CREDS_UNIVERSE_DOMAIN"),
		)
		if err != nil {
			display.ExitError(err.Error(), 20)
		}
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(
			gdrive,
			os.Getenv("GSPREADSHEET_ID"),
		)
		err = gspreadsheet.InitSpreadsheet()
		if err != nil {
			display.ExitError(err.Error(), 10)
		}
		err = gspreadsheet.AddSpreadsheet("MySXCollector-test", true)
		if err != nil {
			display.Error(err.Error())
		} else {
			sheetID, err := gspreadsheet.UpsertSheet("My testing Sheet")
			if err != nil {
				display.Error(err.Error())
			} else {
				display.Debug(fmt.Sprintf("The sheetID is : %v", sheetID))
			}
		}
		display.Debug(fmt.Sprintf("The spreadsheetID is : %v", gspreadsheet.SpreadsheetID))
	case "test-confluence":
		conf := sxConfluence.NewConfluence(
			os.Getenv("CONFLUENCE_BASE_URL"),
			os.Getenv("CONFLUENCE_USERNAME"),
			os.Getenv("CONFLUENCE_API_TOKEN"),
			os.Getenv("CONFLUENCE_SPACE_KEY"),
		)
		ConfluencePage := sxConfluence.NewConfluencePage(
			os.Getenv("CONFLUENCE_SPACE_KEY"),
			os.Getenv("CONFLUENCE_PAGE_ID"),
			os.Getenv("CONFLUENCE_PARENT_ID"),
			os.Getenv("CONFLUENCE_PAGE_TITLE"),
			os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT"),
		)
		httpResp, err := conf.CreateOrAppendPage(ConfluencePage, "<hr/><h1>test</h1>")
		if err != nil {
			display.Error(err.Error())
		} else {
			display.Debug(fmt.Sprintf("HTTP Response: %v", httpResp))
		}
	default:
		sxUtils.DisplayHelpCmd()
	}
}
