# go-libs [![release](https://img.shields.io/badge/release-v0.2.5-blue.svg)](https://gitlab.com/startx1/k8s/go-libs/-/releases/v0.2.5) [![last commit](https://img.shields.io/gitlab/last-commit/startx1/k8s/go-libs.svg)](https://gitlab.com/startx1/k8s/go-libs) [![Doc](https://readthedocs.org/projects/sxgolib/badge)](https://sxgolib.readthedocs.io) 

This project is focused on delivering a set of go tools used into various startx go modules.

## Getting started

in your go code import the go-lib module

```go
import (
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)
```

Use the module in your code

```go

// Use the module in your code
func main() {
	display := sxUtils.NewCmdDisplay("main")
	display.Debug("Display a debug message")
}
```

Here is a list of all the package present in this module

- [Install the `go-libs` binary](https://sxgolib.readthedocs.io/en/devel/installation/) [(download)](https://gitlab.com/startx1/k8s/go-libs/-/raw/stable/bin/sxlibs)
- Test it with `sxlibs version`
- Log into a kubernetes cluster `kubectl login ...`
- Create a new quotas based on the default template `sxlibs create myquotas`
- Get your quotas to see the changes `kubectl get resourcequotas myquotas -o yaml`
- Resize the quotas definitions `sxlibs resize myquotas 3`
- Get your quotas to see the changes `kubectl get resourcequotas myquotas -o yaml`
- Adjust the quotas definitions `sxlibs adjust myquotas`
- Get your quotas to see the changes `kubectl get resourcequotas myquotas -o yaml`
