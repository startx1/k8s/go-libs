# Installation

## Getting started

in your go code import the go-lib module

```go
import (
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)
```

Use the module in your code

```go

// Use the module in your code
func main() {
	display := sxUtils.NewCmdDisplay("main")
	display.Debug("Display a debug message")
}
```