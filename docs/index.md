# Startx GO Libs

[![release](https://img.shields.io/badge/release-v0.2.5-blue.svg)](https://gitlab.com/startx1/k8s/go-libs/-/releases/v0.2.5) [![last commit](https://img.shields.io/gitlab/last-commit/startx1/k8s/go-libs.svg)](https://gitlab.com/startx1/k8s/go-libs) [![Doc](https://readthedocs.org/projects/sxgolib/badge)](https://sxgolib.readthedocs.io) 

The `sxlibs` library containt a list of generic packages used by all go project used by startx.

## Getting started

in your go code import the go-lib module

```go
import (
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)
```

Use the module in your code

```go

// Use the module in your code
func main() {
	display := sxUtils.NewCmdDisplay("main")
	display.Debug("Display a debug message")
}
```

## Contributing

You can help us by [contributing to the project](contribute.md).


## History and releases

Read history [traceback](history.md) for more information on change and released version. 
