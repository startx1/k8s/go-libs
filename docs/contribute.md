# Building environment

In order to get the full developement environment, you must stick to the following procedure

## 0. Requirements

- Having a RedHat like (Fedora, CentOS, RHEL, ...) operating system
- Install git with `sudo yum install git`
- Install helm with `sudo yum install go`
- Install a kubernetes client
  - openshift client with `sudo yum install openshift-client`
  - kubernetes client with `sudo yum install kubectl`

## 1. Get a copy of the repository

```bash
git clone https://gitlab.com/startx1/k8s/go-libs.git
cd go-libs
git checkout devel
git checkout -b devel-myfeature
```

## 2. Generate the binary

Create a new sxlibs binary from source.

```bash
make test
```

## 3. Publish your changes

Publish all your change to the repository. Only publish to branch derivated from the `devel` branch.

```bash
git add .
git commit -m "my change to the repository"
git push origin devel-myfeature
```

## 4. Create a pull request

Create a pull-request from your `devel-myfeature` into the `devel` branch.
