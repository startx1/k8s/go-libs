# Release history

## version 1.0.x (champagnac)

_The objectif of this release is to stabilize the full repository content and offer a stable release of sxlibs._

## version 0.4.x (chaumeil)

_The objectif of this release is to benefit from feedback and focus on creating a small community rhythm._

## version 0.3.x (chassagne)

_The objectif of this release is to communicate about the availability of this application._

## version 0.2.x (chastang)

The objectif of this release is to provide integration with the kubectl krew plugin mechanism, as well
as rpm, deb, homebrew, snap and container version.

### History

| Release                                                       | Date       | Description                                                    |
| ------------------------------------------------------------- | ---------- | -------------------------------------------------------------- |
| [0.2.5](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.2.5) | 2025-03-10 | Update all dependencies                                        |
| [0.2.3](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.2.3) | 2024-11-13 | Add support for a second key in prometheus MultiQuery response |
| [0.2.1](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.2.1) | 2024-11-12 | Upgrade to go version 1.23.3                                   |

## version 0.1.x (chauzu)

The objectif of this release is to create the repository structure and fundamentals of the project.

### History

| Release                                                        | Date       | Description                                             |
| -------------------------------------------------------------- | ---------- | ------------------------------------------------------- |
| [0.1.17](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.17) | 2024-11-05 | Upgrade all packages for security fix                   |
| [0.1.15](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.15) | 2024-10-17 | Add the gdrive package                                  |
| [0.1.11](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.11) | 2024-10-15 | Enable the unauthenticated SMTP relay                   |
| [0.1.9](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.9)   | 2024-10-13 | Improve the confluence lib                              |
| [0.1.7](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.7)   | 2024-10-12 | Imporve check for confluence append and update features |
| [0.1.5](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.5)   | 2024-10-12 | Adding the confluence package                           |
| [0.1.3](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.3)   | 2024-10-11 | Imporve the smtp-insecure option                        |
| [0.1.1](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.1)   | 2024-10-06 | Minor improvments in Kubeconfig and k8client            |
| [0.1.0](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.1.0)   | 2024-10-06 | First release of the 0.1.x series                       |

## version 0.0.x (champeaux)

This version is a POC for testing purpose.

The objectif of this release is to create the repository structure and fundamentals of the project.

### History

| Release                                                        | Date       | Description                                                                                                 |
| -------------------------------------------------------------- | ---------- | ----------------------------------------------------------------------------------------------------------- |
| [0.0.25](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.25) | 2024-10-04 | Debug mailer package                                                                                        |
| [0.0.23](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.23) | 2024-10-04 | Add the mailer package                                                                                      |
| [0.0.21](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.21) | 2024-10-02 | Add the prometheus package                                                                                  |
| [0.0.19](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.19) | 2024-10-01 | Update go dependencies and introduce the NewOCPClientSet method to interact with OCP cluster instead of K8S |
| [0.0.17](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.17) | 2024-09-24 | Update all go dependencies for security considerations                                                      |
| [0.0.15](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.15) | 2024-09-11 | Update the go version to 1.23.1                                                                             |
| [0.0.11](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.11) | 2024-09-10 | Adding the GetConfig method to K8SClient                                                                    |
| [0.0.9](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.9)   | 2024-05-08 | Upgrade kubeconfig config                                                                                   |
| [0.0.7](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.7)   | 2024-05-03 | move module to scope `gitlab.com/startx1/k8s/go-libs`                                                       |
| [0.0.5](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.5)   | 2024-05-03 | Adding documentation and test binary production                                                             |
| [0.0.3](https://gitlab.com/startx1/k8s/go-libs/-/tags/0.0.3)   | 2024-05-03 | Initial package                                                                                             |
