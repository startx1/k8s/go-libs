# sxlibs version

return the version of the sxlibs binary.

## Usage

```bash
sxlibs version [OPTIONS]...
```

## Arguments

_no arguments for version_

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

Display the version of this binary.

```bash
sxlibs version
```