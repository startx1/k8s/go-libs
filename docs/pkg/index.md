# Using sxlibs

## Sub-command list

- [Generate a resourceQuota based on a template](generate.md)
- [Create a resourceQuota based on a template](create.md)
- [Apply a template to an existing resourceQuota](apply.md)
- [Resize a resourceQuota with increment](resize.md)
- [Adjust a resourceQuota with it's status.used](adjust.md)
- [Describe resourceQuotas content from namespaces](describe.md)
- [Export resourceQuotas content from namespaces](export.md)
- [List all availables templates](templates.md)
- [Get detail about a template](template.md)
- [Get the sxlibs version](version.md)
