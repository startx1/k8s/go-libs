# go-libs : confluence

This package allow you to manipulate a Confluence page and interact with it.

## ConfluencePage object

ConfluencePage object describ the content of a confluence page.

| Property     | Kind                  | Description                             |
| ------------ | --------------------- | --------------------------------------- |
| SpaceKey     | `string`              | The confluence space key                |
| PageID       | `string`              | The confluence pageID                   |
| ParentPageID | `string`              | The confluence parent pageID            |
| Title        | `string`              | The confluence page title               |
| HtmlContent  | `string`              | The confluence page content             |
| display      | `*sxUtils.CmdDisplay` | Display context used for debug purposes |


### Constructor

NewConfluencePage create a new page definition ready to use with the Confluence object.

```bash
export CONFLUENCE_SPACE_KEY='MYSPACE'
export CONFLUENCE_PARENT_ID='123456'  
export CONFLUENCE_PAGE_ID='12345678'
export CONFLUENCE_PAGE_TITLE="my example page"
export CONFLUENCE_PAGE_HTML_CONTENT="my example content"
```

```go
confluencePage := sxConfluence.NewConfluencePage(
    os.Getenv("CONFLUENCE_SPACE_KEY"),
    os.Getenv("CONFLUENCE_PAGE_ID"),
    os.Getenv("CONFLUENCE_PARENT_ID"),
    os.Getenv("CONFLUENCE_PAGE_TITLE"),
    os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT"),
)
```

Return an `*ConfluencePage` object.


### ConfluencePage methods

ConfluencePage object propose a full set of methods.

| Method           | Signature        | Return            | Description                                               |
| ---------------- | ---------------- | ----------------- | --------------------------------------------------------- |
| GetNewPayload    | _none_           | `[]byte`, `error` | Return a new Json playload for a creating page definition |
| GetUpdatePayload | newVersion `int` | `[]byte`, `error` | Return a new Json playload for a update page definition   |


## Confluence object

Confluence object describe content and acces to the confluence content.

| Property     | Kind                  | Description                                                                              |
| ------------ | --------------------- | ---------------------------------------------------------------------------------------- |
| BaseURL      | `string`              | The base URL of your confluence instance                                                 |
| Username     | `string`              | The confluence username                                                                  |
| ApiToken     | `string`              | The confluence APIToken. see https://id.atlassian.com/manage-profile/security/api-tokens |
| SpaceKey     | `string`              | The confluence SpaceKey                                                                  |
| ParentPageID | `string`              | The ParentPageID (optional)                                                              |
| display      | `*sxUtils.CmdDisplay` | Display context used for debug purposes                                                  |


### Constructor

NewConfluence create a new Confluence ready to use and interact with a
remote confluence instance and space.

```bash
export CONFLUENCE_BASE_URL='https://example.atlassian.net/wiki'
export CONFLUENCE_USERNAME='user@example.com'
export CONFLUENCE_API_TOKEN='mytoken' 
export CONFLUENCE_SPACE_KEY='MYSPACE'
export CONFLUENCE_PARENT_ID='123456'  
export CONFLUENCE_PAGE_ID='12345678'
export CONFLUENCE_PAGE_TITLE="my example title"
export CONFLUENCE_PAGE_HTML_CONTENT="my example body"
```

```go
confluence := sxConfluence.NewConfluence(
    os.Getenv("CONFLUENCE_BASE_URL"),
    os.Getenv("CONFLUENCE_USERNAME"),
    os.Getenv("CONFLUENCE_API_TOKEN"),
    os.Getenv("CONFLUENCE_SPACE_KEY"),
)
```

Return an `*Confluence` object.


### Confluence methods

Confluence object propose a full set of methods.

| Method             | Signature                                | Return                                               | Description                                              |
| ------------------ | ---------------------------------------- | ---------------------------------------------------- | -------------------------------------------------------- |
| _GET               | url `string`                             | `*http.Response`, `string`, `*http.Request`, `error` | Execute a GET request to confluence                      |
| _POST              | url `string`, payloadBytes `[]byte`      | `*http.Response`, `string`, `*http.Request`, `error` | Execute a POST request to confluence                     |
| _PUT               | url `string`, payloadBytes `[]byte`      | `*http.Response`, `string`, `*http.Request`, `error` | Execute a PUT request to confluence                      |
| _DELETE            | url `string`                             | `*http.Response`, `string`, `*http.Request`, `error` | Execute a DELETE request to confluence                   |
| ExtractPageVersion | response `*http.Response`, body `string` | `int`, `error`                                       | Extract the PageVersion from a confluencePage definition |
| ExtractPageContent | response `*http.Response`, body `string` | `int`, `error`                                       | Extract the PageContent from a confluencePage definition |
| CreatePage         | page `*ConfluencePage`                   | `*http.Response`, `error`                            | Create a confluence page                                 |
| UpdatePage         | page `*ConfluencePage`                   | `*http.Response`, `error`                            | Update a confluence page                                 |
| AppendPage         | page `*ConfluencePage`, sep `string`     | `*http.Response`, `error`                            | Append content of a confluence page                      |
| DeletePage         | page `*ConfluencePage`                   | `bool`, `error`                                      | Delete a confluence page                                 |

#### Create a new page

Example in order to create a new confluence page.

```go
confluencePage := sxConfluence.NewConfluencePage(
    os.Getenv("CONFLUENCE_SPACE_KEY"),
    "",
    os.Getenv("CONFLUENCE_PARENT_ID"),
    os.Getenv("CONFLUENCE_PAGE_TITLE"),
    os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT"),
)
confluence := sxConfluence.NewConfluence(
    os.Getenv("CONFLUENCE_BASE_URL"),
    os.Getenv("CONFLUENCE_USERNAME"),
    os.Getenv("CONFLUENCE_API_TOKEN"),
    os.Getenv("CONFLUENCE_SPACE_KEY"),
)
httpResp, err := confluence.CreatePage(confluencePage)
if err != nil {
    display.Error(err.Error())
} else {
    display.Debug(fmt.Sprintf("HTTP Response: %v", httpResp))
}
```

#### Update an existing page

Example in order to update an existing confluence page.

```go
confluencePage := sxConfluence.NewConfluencePage(
    os.Getenv("CONFLUENCE_SPACE_KEY"),
    os.Getenv("CONFLUENCE_PAGE_ID"),
    "",
    os.Getenv("CONFLUENCE_PAGE_TITLE"),
    os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT"),
)
confluence := sxConfluence.NewConfluence(
    os.Getenv("CONFLUENCE_BASE_URL"),
    os.Getenv("CONFLUENCE_USERNAME"),
    os.Getenv("CONFLUENCE_API_TOKEN"),
    os.Getenv("CONFLUENCE_SPACE_KEY"),
)
httpResp, err := confluence.UpdatePage(confluencePage)
if err != nil {
    display.Error(err.Error())
} else {
    display.Debug(fmt.Sprintf("HTTP Response: %v", httpResp))
}
```

#### Append an existing page

Example in order to append content into an existing confluence page.

```go
confluencePage := sxConfluence.NewConfluencePage(
    os.Getenv("CONFLUENCE_SPACE_KEY"),
    os.Getenv("CONFLUENCE_PAGE_ID"),
    "",
    os.Getenv("CONFLUENCE_PAGE_TITLE"),
    os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT"),
)
confluence := sxConfluence.NewConfluence(
    os.Getenv("CONFLUENCE_BASE_URL"),
    os.Getenv("CONFLUENCE_USERNAME"),
    os.Getenv("CONFLUENCE_API_TOKEN"),
    os.Getenv("CONFLUENCE_SPACE_KEY"),
)
httpResp, err := confluence.AppendPage(confluencePage,"<hr/><h1>My appended section</h1>")
if err != nil {
    display.Error(err.Error())
} else {
    display.Debug(fmt.Sprintf("HTTP Response: %v", httpResp))
}
```

#### Delete an existing page

Example in order to delete content into an existing confluence page.

```go
confluencePage := sxConfluence.NewConfluencePage(
    os.Getenv("CONFLUENCE_SPACE_KEY"),
    os.Getenv("CONFLUENCE_PAGE_ID"),
    "",
    os.Getenv("CONFLUENCE_PAGE_TITLE"),
    os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT"),
)
confluence := sxConfluence.NewConfluence(
    os.Getenv("CONFLUENCE_BASE_URL"),
    os.Getenv("CONFLUENCE_USERNAME"),
    os.Getenv("CONFLUENCE_API_TOKEN"),
    os.Getenv("CONFLUENCE_SPACE_KEY"),
)
_, err := confluence.DeletePage(confluencePage)
if err != nil {
    display.Error(err.Error())
}
```
