# go-libs : k8sclient

This package allow you to manipulate a client-go connection to a cluster.

## K8sClient object

K8sClient object represent the holder for the client-go connection.

| Property       | Kind                    | Description                             |
| -------------- | ----------------------- | --------------------------------------- |
| Clientset      | `*kubernetes.Clientset` | The kubernetes Clientset                |
| kubeconfigpath | `string`                | The kubernetes kubeconfig               |
| display        | `*sxUtils.CmdDisplay`   | Display context used for debug purposes |

## Constructor

NewK8sClient create a new K8sClient ready to use with a clientset
prepared to interact with the kubernetes cluster.

```go
KClient  := NewK8sClient("/home/user/.kube/config")
KClient.Debug()
```

Return an `*K8sClient` object.


## K8sClient methods

K8sClient object propose a full set of methods.

| Method                    | Signature        | Return              | Description                                                |
| ------------------------- | ---------------- | ------------------- | ---------------------------------------------------------- |
| Debug                     | _none_           | `*K8sClient`        | Display in debug mode the content of the k8sclient         |
| IsConnected               | `bool`           | `*K8sClient`        | Returns if the k8sclient is connected                      |
| GetKubeconfigPath         | _none_           | `string`            | Return the kubeconfig path                                 |
| GetCurrentNamespace       | _none_           | `string`            | Return the name of the current namespace                   |
| ListAllNamespaces         | _none_           | `[]string`, `error` | Return the list of all the namespaces                      |
| ListAllMatchingNamespaces | pattern `string` | `[]string`, `error` | Return the list of all the namespaces matching the pattern |

