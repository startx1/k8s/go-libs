# go-libs : mailer

This package allow you to manipulate a Mailer and interact with it.

## Mailer object

Mailer object describe content and acces to the mailer content.

| Property     | Kind                  | Description                             |
| ------------ | --------------------- | --------------------------------------- |
| SmtpHost     | `string`              | The SMTP host                           |
| SmtpPort     | `string`              | The SMTP port                           |
| SmtpUsername | `string`              | The SMTP username                       |
| SmtpPassword | `string`              | The SMTP password                       |
| display      | `*sxUtils.CmdDisplay` | Display context used for debug purposes |


### Constructor

NewMailer create a new Mailer ready to use with a clientset
prepared to interact with the kubernetes cluster.

```go
// example with unauthenticated SMTP relay
Mailer  := NewMailer("smtp.example.com", "25", "", "")
// example with authenticated SMTP relay
Mailer  := NewMailer("smtp.example.com", "587", "user", "123456")
Mailer.Send(mail)
```

Return an `*Mailer` object.


### Mailer methods

Mailer object propose a full set of methods.

| Method | Signature                       | Return  | Description    |
| ------ | ------------------------------- | ------- | -------------- |
| Send   | mail *MailerMail, insecure bool | `error` | Send the email |

## MailerMail object

MailerMail object describ the content of a mail

| Property    | Kind                     | Description                             |
| ----------- | ------------------------ | --------------------------------------- |
| From        | `string`                 | email of the sender                     |
| To          | `string`                 | email of the receiver                   |
| Subject     | `string`                 | Subject of this message                 |
| Body        | `string`                 | Content of the message                  |
| Attachments | `[]MailerMailAttachment` | List of attachments                     |
| display     | `*sxUtils.CmdDisplay`    | Display context used for debug purposes |


### Constructor

NewMailerMail create a new message ready to use with the Mailer.

```go
mail  := NewMailerMail(
    "from@startx.fr", 
    "to@startx.fr", 
    "message title", 
    "message content",
    )
mail.AddAttachment("notes.txt", []byte("This is a text file with some notes."))
// example with secure SMTP relay
mail.Send(mailer, false)
// example with insecure SMTP relay
mail.Send(mailer, true)
```

Return an `*MailerMail` object.


### MailerMail methods

MailerMail object propose a full set of methods.

| Method        | Signature                               | Return        | Description                       |
| ------------- | --------------------------------------- | ------------- | --------------------------------- |
| SetFrom       | from `string`                           | `*MailerMail` | Define the sender                 |
| SetTo         | to `string`                             | `*MailerMail` | Define the receiver               |
| SetSubject    | subject `string`                        | `*MailerMail` | Define the subject of the message |
| SetBody       | message `string`                        | `*MailerMail` | Define the body of the message    |
| AddAttachment | filename `string`, filecontent `[]byte` | `*MailerMail` | Adding an attachment              |
| Send          | mail *Mailer, insecure bool             | `*MailerMail` | Send the email using the Mailer   |
