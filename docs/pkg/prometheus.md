# go-libs : prometheus

This package allow you to manipulate a prometheus backend and interact with it.

## Prometheus object

Prometheus object describe content and acces to the prometheus content.

| Property | Kind                  | Description                             |
| -------- | --------------------- | --------------------------------------- |
| URL      | `string`              | The URL to the prometheus backend       |
| Token    | `string`              | The Token to the prometheus backend     |
| kclient  | `*sxKCli.K8sClient`   | The kubernetes client to use            |
| display  | `*sxUtils.CmdDisplay` | Display context used for debug purposes |

## Constructor

NewPrometheus create a new Prometheus ready to use with a rest API
prepared to interact with the prometheus backend.

```go
Prometheus  := NewPrometheus()
Prometheus.SetPrometheusOCP()
Prometheus.Debug()
```

Return an `*Prometheus` object set with an openshift backend.


## Prometheus methods

Prometheus object propose a full set of methods.

| Method                      | Signature                          | Return                | Description                                                              |
| --------------------------- | ---------------------------------- | --------------------- | ------------------------------------------------------------------------ |
| Debug                       | _none_                             | `*Prometheus`         | Display in debug mode the content of the prometheus                      |
| SetPrometheusOCP            | _none_                             | `error`               | Set URL and Token with Openshift setup                                   |
| FindPrometheusOCPRoute      | ns, route                          | `string`, `error`     | Return URL for the Openshift public route                                |
| FindPrometheusOCPSAToken    | ns, sa                             | `string`, `error`     | Return Token for the currently coonected used                            |
| GetPrometheusConnection     | insecure                           | `promv1.API`, `error` | Return a connection to a prometheus server                               |
| ExecPrometheusQuery         | promQuery                          | `[][]string`, `error` | Return a result from a single prometheus query                           |
| ExecPrometheusMultipleQuery | promQueries, start, stop, duration | `[][]string`, `error` | Return a result list for all queries                                     |
| QueryParseModelMetric       | query, sample, key                 | xx, `error`           | Return a result list for min, max, first, last, avg, sum of a metric key |



