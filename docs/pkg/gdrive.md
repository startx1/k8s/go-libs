# go-libs : gdrive

This package allow you to manipulate a GDrive file and interact with google drive API
using a serviceaccount.

## GDrive object

GDrive object describe content and access to the gdrive API.

| Property           | Kind                  | Description                                                                  |
| ------------------ | --------------------- | ---------------------------------------------------------------------------- |
| CredentialFilePath | `string`              | Full path to the credential file                                             |
| CredentialFileBase | `string`              | The directory where is located the credentails file                          |
| CredentialFileName | `string`              | The filename of the credential file (default is credentials.json)            |
| Domain             | `string`              | The company domain (used if whant to enable permissions at the domain level) |
| display            | `*sxUtils.CmdDisplay` | Display context used for debug purposes                                      |

### Constructor

NewGDrive create a new GDrive ready to use and interact with a
remote gdrive instance and space.

```bash
export GDRIVE_CREDENTIAL_FILENAME='credentials.json'
export GDRIVE_CREDENTIAL_PATH='/tmp'
export GDRIVE_CREDENTIAL_DOMAIN='example.com'
```

```go
gdrive := sxGDrive.NewGDrive(
    os.Getenv("GDRIVE_CREDENTIAL_FILENAME"),
    os.Getenv("GDRIVE_CREDENTIAL_PATH"),
    os.Getenv("GDRIVE_CREDENTIAL_DOMAIN"),
)
```

Return an `*GDrive` object.


### GDrive methods

GDrive object propose a full set of methods.

| Method                | Signature                                                                                                                                                                                                                                                                    | Return    | Description                                                           |
| --------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | --------------------------------------------------------------------- |
| SetCredentialsFile    | fileName `string`, fileDir `string`                                                                                                                                                                                                                                          | `*GDrive` | Setup the credentials file informations                               |
| SetCredentialsContent | typeName `string`, project_id `string`, private_key_id `string`, private_key `string`, client_email `string`, client_id `string`, auth_uri `string`, token_uri `string`, auth_provider_x509_cert_url `string`, client_x509_cert_url`string`, universe_domain string `string` | `error`   | Creaaate a new credentials file accoridng to all the given parameters |
| CheckCredentials      | _none_                                                                                                                                                                                                                                                                       | `error`   | Check if the credentials file exist and if it can reach Google API    |


#### Create a credential file

Example in order to create a new gdrive credentials file using the serviceaccount structure.


```bash
export GDRIVE_CREDENTIAL_FILENAME='credentials.json'
export GDRIVE_CREDENTIAL_PATH='/tmp'
export GDRIVE_CREDENTIAL_DOMAIN='example.com'
export GDRIVE_CREDS_TYPE="service_account"
export GDRIVE_CREDS_PROJECT_ID="myproject"
export GDRIVE_CREDS_PRIVATE_KEY_ID="ca123456aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
export GDRIVE_CREDS_PRIVATE_KEY="-----BEGIN PRIVATE KEY-----\nPUT_YOUR_KEY_HERE\n-----END PRIVATE KEY-----\n"
export GDRIVE_CREDS_CLIENT_EMAIL="mysaaccount@myproject.iam.gserviceaccount.com"
export GDRIVE_CREDS_CLIENT_ID="111111111111111111111"
export GDRIVE_CREDS_AUTH_URI="https://accounts.google.com/o/oauth2/auth"
export GDRIVE_CREDS_TOKEN_URI="https://oauth2.googleapis.com/token"
export GDRIVE_CREDS_AUTH_PROVIDER_X509_CERT_URL="https://www.googleapis.com/oauth2/v1/certs"
export GDRIVE_CREDS_CLIENT_X509_CERT_URL="https://www.googleapis.com/robot/v1/metadata/x509/mysaaccount%40myproject.iam.gserviceaccount.com"
export GDRIVE_CREDS_UNIVERSE_DOMAIN="googleapis.com"
```

```go
gdrive := sxGDrive.NewGDrive(
    os.Getenv("GDRIVE_CREDENTIAL_FILENAME"),
    os.Getenv("GDRIVE_CREDENTIAL_PATH"),
    os.Getenv("GDRIVE_CREDENTIAL_DOMAIN"),
)
err := gdrive.SetCredentialsContent(
    os.Getenv("GDRIVE_CREDS_TYPE"),
    os.Getenv("GDRIVE_CREDS_PROJECT_ID"),
    os.Getenv("GDRIVE_CREDS_PRIVATE_KEY_ID"),
    os.Getenv("GDRIVE_CREDS_PRIVATE_KEY"),
    os.Getenv("GDRIVE_CREDS_CLIENT_EMAIL"),
    os.Getenv("GDRIVE_CREDS_CLIENT_ID"),
    os.Getenv("GDRIVE_CREDS_AUTH_URI"),
    os.Getenv("GDRIVE_CREDS_TOKEN_URI"),
    os.Getenv("GDRIVE_CREDS_AUTH_PROVIDER_X509_CERT_URL"),
    os.Getenv("GDRIVE_CREDS_CLIENT_X509_CERT_URL"),
    os.Getenv("GDRIVE_CREDS_UNIVERSE_DOMAIN"),
)
if err != nil {
    display.ExitError(err.Error(), 20)
}
```

## GDriveSpreadsheet object

GDriveSpreadsheet object describ the content of a google spreadsheet

| Property      | Kind                  | Description                                                    |
| ------------- | --------------------- | -------------------------------------------------------------- |
| GDrive        | `*GDrive`             | The gdrive service used to interact with gdrive API            |
| Service       | `*sheets.Service`     | The google sheet service used to interact with spreadsheet API |
| SpreadsheetID | `string`              | The SpreadsheetID of the spreadsheet                           |
| Spreadsheet   | `*sheets.Spreadsheet` | The Spreadsheet object for this spreadsheet                    |
| display       | `*sxUtils.CmdDisplay` | Display context used for debug purposes                        |


### Constructor

NewGDriveSpreadsheet create a new page definition ready to use with the GDrive object.

```bash
export GSPREADSHEET_ID='XXXXXXXXXXXXXXXXXXXXX_xxxxxxxxxxxxxx-XXXXXXX'
export GDRIVE_CREDENTIAL_FILENAME='credentials.json'
export GDRIVE_CREDENTIAL_PATH='/tmp'
export GDRIVE_CREDENTIAL_DOMAIN='example.com'
```

```go
gdrive := sxGDrive.NewGDrive(
    os.Getenv("GDRIVE_CREDENTIAL_FILENAME"),
    os.Getenv("GDRIVE_CREDENTIAL_PATH"),
    os.Getenv("GDRIVE_CREDENTIAL_DOMAIN"),
)
gspreadsheet := sxGDrive.NewGDriveSpreadsheet(
    gdrive,
    os.Getenv("GSPREADSHEET_ID"),
)
err = gspreadsheet.InitSpreadsheet()
if err != nil {
    display.ExitError(err.Error(), 10)
}
```

Return an `*GDriveSpreadsheet` object.


### GDriveSpreadsheet methods

GDriveSpreadsheet object propose a set of methods to interact with a google spreadsheet.

| Method                | Signature                       | Return                         | Description                                                 |
| --------------------- | ------------------------------- | ------------------------------ | ----------------------------------------------------------- |
| InitSpreadsheet       | _none_                          | `error`                        | Set SpreadsheetService and load Spreadsheet if an ID is set |
| SetSpreadsheetService | _none_                          | `error`                        | Create the spreadsheet NewService                           |
| AddSpreadsheet        | title `string`, isDomain `bool` | `error`                        | Add a new spreadsheet pirvate or for the domain             |
| GetSpreadsheet        | spreadsheetID `string`          | `*sheets.Spreadsheet`, `error` | Get the content of a spreadsheet                            |
| LoadSpreadsheet       | _none_                          | `error`                        | Get the content of a spreadsheet and store it internally    |

| GetSheetByTitle | title `string` | `int64`,`error` | Get the ID of a sheet coresponding to a title  |
| AddSheet | title `string` | `int64`,`error` | Add a new sheet  |
| UpsertSheet | title `string` | `int64`,`error` | Return the sheet ID coresponding to the title or create a new one  |

