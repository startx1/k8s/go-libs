# go-libs : kubeconfig

This package allow you to manipulate a Kubeconfig and interact with it.

## Kubeconfig object

Kubeconfig object describe content and acces to the kubeconfig content.

| Property | Kind                  | Description                             |
| -------- | --------------------- | --------------------------------------- |
| Config   | `string`              | The content of the kubeconfig           |
| Path     | `string`              | The path to the kubeconfig file         |
| display  | `*sxUtils.CmdDisplay` | Display context used for debug purposes |

## Constructor

NewKubeConfig create a new Kubeconfig ready to use with a clientset
prepared to interact with the kubernetes cluster.

```go
Kubeconfig  := NewKubeConfig()
Kubeconfig.LoadFromHome()
Kubeconfig.Debug()
```

Return an `*Kubeconfig` object.


## Kubeconfig methods

Kubeconfig object propose a full set of methods.

| Method         | Signature         | Return                 | Description                                         |
| -------------- | ----------------- | ---------------------- | --------------------------------------------------- |
| Debug          | _none_            | `*Kubeconfig`          | Display in debug mode the content of the kubeconfig |
| LoadFromFile   | filePath `string` | `*Kubeconfig`, `error` | Loading Kubeconfig from a file                      |
| LoadFromEnv    | _none_            | `*Kubeconfig`, `error` | Loading Kubeconfig from the KUBECONFIG env var      |
| LoadFromHome   | _none_            | `*Kubeconfig`, `error` | Loading Kubeconfig from the home directory          |
| LoadFromYaml   | `[]byte`          | `*Kubeconfig`          | Loading Kubeconfig from a raw yaml content          |
| LoadFromString | `string`          | `*Kubeconfig`          | Loading Kubeconfig from a YAML string content       |
| Load           | _none_            | `*Kubeconfig`          | Automatic load of the kubeconfig content            |
| GetConfig      | _none_            | `string`               | Return the kubeconfig content                       |
| GetConfigBytes | _none_            | `[]byte`, `error`      | Return the kubeconfig content in bytes              |
| GetYaml        | _none_            | `string`               | Return the kubeconfig content                       |
| GetPath        | _none_            | `string`               | Return the kubeconfig file path                     |