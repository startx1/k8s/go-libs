# go-libs : incrementor

This package allow you to manipulate a Resource.Quantity object and increment it.

## Incrementor object

Incrementor object describe a value with incrementation capacity 
according to the Unit type.

| Property     | Kind                  | Description                                  |
| ------------ | --------------------- | -------------------------------------------- |
| String       | `string`              | The string representation of the incrementor |
| Int          | `int`                 | The int representation of the incrementor    |
| Qty          | `string`              | The unit of quantity                         |
| valType      | `string`              | The type of value (count, mem or cpu)        |
| valUnit      | `string`              | The unit applyed to the value                |
| isPercentage | `bool`                | Is the value representing a percentage       |
| display      | `*sxUtils.CmdDisplay` | Display context used for debug purposes      |


## Constructor

NewIncrementor create a new Incrementor ready to use the clientset
to interact with the kubernetes cluster

```go
incrementMem   := NewIncrementor("6Gi")
incrementCpu   := NewIncrementor("600m")
incrementCount := NewIncrementor("6")
incrementMem.Debug()
incrementCpu.Debug()
incrementCount.Debug()
```

Return an `*Incrementor` object.


## Incrementor methods

Incrementor object propose a full set of methods.

| Method           | Signature               | Return              | Description                                          |
| ---------------- | ----------------------- | ------------------- | ---------------------------------------------------- |
| LoadFromString   | val `string`            | `*Incrementor`      | Load the incrementor from a string                   |
| Debug            | _none_                  | `*Incrementor`      | Display in debug mode the content of the incrementor |
| SetIsPercentage  | isPercentage `bool`     | `*Incrementor`      | Define the incrementor as a percentage               |
| IsPercentage     | `bool`                  | `*Incrementor`      | Returns if the incrementor is a percentage           |
| SetValString     | val `string`            | `*Incrementor`      | Set the valString field                              |
| GetValString     | _none_                  | `string`            | Get the valString field                              |
| SetValInt        | val `string`            | `*Incrementor`      | Set the valInt field                                 |
| GetValInt        | _none_                  | `int`               | Get the valInt field                                 |
| SetValQty        | val `resource.Quantity` | `*Incrementor`      | Set the Incrementor from resource.Quantity           |
| GetValQty        | _none_                  | `resource.Quantity` | Get the resource.Quantity representation             |
| SetValType       | val `string`            | `*Incrementor`      | Set the ValType field                                |
| GetValType       | _none_                  | `string`            | Get the ValType field                                |
| SetValUnit       | val `string`            | `*Incrementor`      | Set the ValUnit field                                |
| GetValUnit       | _none_                  | `string`            | Get the ValUnit field                                |
| GetValStringUnit | _none_                  | `string`            | valString + valUnit field.                           |

