package gdrive

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	"google.golang.org/api/option"
	"google.golang.org/api/transport"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName                                      = "gdrive"
	errCheckCredentialsVerifReqCreate              = "unable to create verification request: %v"
	errCheckCredentialsVerifReqNok                 = "credentials verification failed: %v"
	dbgCheckCredentialsVerifOK                     = "credentials verified successfully"
	dbgSetCredentialsFileOK                        = "set credentials file to %s"
	dbgSetCredentialsContentStart                  = "start generating a credentials file to %s"
	dbgSetCredentialsContentOK                     = "save credentials to file %s"
	wrnSetCredentialsContentNoType                 = "no type found use default %s"
	wrnSetCredentialsContentNoClientId             = "no client_id found use default %s"
	wrnSetCredentialsContentNoProjectId            = "no project_id found use default %s"
	wrnSetCredentialsContentNoPrivateKeyId         = "no private_key_id found use default %s"
	wrnSetCredentialsContentNoPrivateKey           = "no private_key found use default"
	wrnSetCredentialsContentNoClientEmail          = "no client_email found use default %s"
	wrnSetCredentialsContentNoAuthUri              = "no auth_uri found use default %s"
	wrnSetCredentialsContentNoTokenUri             = "no token_uri found use default %s"
	wrnSetCredentialsContentNoAuthProviderX509Cert = "no auth_provider_x509_cert_url found use default %s"
	wrnSetCredentialsContentNoClientX509Cert       = "no client_x509_cert_url found use default %s"
	wrnSetCredentialsContentNoUniverseDomain       = "no universe_domain found use default %s"
	errSetCredentialsContentMarshalNok             = "unable to marshal credentials content: %v"
	dpmType                                        = "service_account"
	dpmPrivateKeyId                                = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
	dpmPrivateKey                                  = "-----BEGIN PRIVATE KEY-----\nPLACE YOU REAL KEY HERE\n-----END PRIVATE KEY-----\n"
	dpmClientEmail                                 = "myserviceaccountname@myproject.iam.gserviceaccount.com"
	dpmClientId                                    = "YOUR_CLIENT_ID.apps.googleusercontent.com"
	dpmProjectId                                   = "YOUR_PROJECT_ID"
	dpmUriAuth                                     = "https://accounts.google.com/o/oauth2/auth"
	dpmUriToken                                    = "https://oauth2.googleapis.com/token"
	dpmUriAuthProviderX509Cert                     = "https://oauth2.googleapis.com/token"
	dpmUriClientX509Cert                           = "https://www.googleapis.com/robot/v1/metadata/x509/nyserviceaccount%40myproject.iam.gserviceaccount.com"
	dpmUniverseDomain                              = "googleapis.com"
	errSetCredentialsContentFileCreateNok          = "unable to create credentials file %s : %v"
	errSetCredentialsContentFileWriteNok           = "unable to write credentials file %s : %v"
	dbgNewGDrive                                   = "Initialize a gdrive object with %s"
)

//
// Define object variables
//

// GDrive is a wrapper around api.Config
type GDrive struct {
	CredentialFilePath string
	CredentialFileBase string
	CredentialFileName string
	Domain             string
	display            *sxUtils.CmdDisplay
}

type SACreds struct {
	Type                string `json:"type"`
	ProjectID           string `json:"project_id"`
	PrivateKeyID        string `json:"private_key_id"`
	PrivateKey          string `json:"private_key"`
	ClientEmail         string `json:"client_email"`
	ClientID            string `json:"client_id"`
	AuthURI             string `json:"auth_uri"`
	TokenURI            string `json:"token_uri"`
	AuthProviderCertURL string `json:"auth_provider_x509_cert_url"`
	ClientCertURL       string `json:"client_x509_cert_url"`
	UniverseDomain      string `json:"universe_domain"`
}

//
// Define constructor and public functions
//

// Initialize a gdrive object
// ex:
//
//	gdrive := kc.NewGDrive()
func NewGDrive(credentialFileName string, credentialFileDir string, domain string) *GDrive {
	return newGDrive(credentialFileName, credentialFileDir, domain)
}

// internal used to construct a new api.Config
func newGDrive(credentialFileName string, credentialFileDir string, domain string) *GDrive {
	display := sxUtils.NewCmdDisplay(GroupName)
	if credentialFileName == "" {
		credentialFileName = "credentials.json"
	}
	if credentialFileDir == "" {
		credentialFileDir = "/tmp"
	}
	credentialFile := credentialFileDir + "/" + credentialFileName
	display.Debug(fmt.Sprintf(dbgNewGDrive, credentialFile))
	return &GDrive{
		CredentialFilePath: credentialFile,
		CredentialFileBase: credentialFileDir,
		CredentialFileName: credentialFileName,
		Domain:             domain,
		display:            display,
	}
}

// Set the credentials file name
func (gdrive *GDrive) SetCredentialsFile(credentialFileName string, credentialFileDir string) *GDrive {
	if credentialFileName == "" {
		credentialFileName = "credentials.json"
	}
	if credentialFileDir == "" {
		credentialFileDir = "/tmp"
	}
	gdrive.CredentialFilePath = credentialFileDir + "/" + credentialFileName
	gdrive.CredentialFileBase = credentialFileDir
	gdrive.CredentialFileName = credentialFileName
	gdrive.display.Debug(fmt.Sprintf(dbgSetCredentialsFileOK, gdrive.CredentialFilePath))
	return gdrive
}

// Set the credentials content from params
func (gdrive *GDrive) SetCredentialsContent(
	typeName string,
	project_id string,
	private_key_id string,
	private_key string,
	client_email string,
	client_id string,
	auth_uri string,
	token_uri string,
	auth_provider_x509_cert_url string,
	client_x509_cert_url string,
	universe_domain string,
) error {
	gdrive.display.Debug(fmt.Sprintf(dbgSetCredentialsContentStart, gdrive.CredentialFilePath))
	if typeName == "" {
		typeName = dpmType
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoType, typeName))
	}
	if project_id == "" {
		project_id = dpmProjectId
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoProjectId, project_id))
	}
	if private_key_id == "" {
		private_key_id = dpmPrivateKeyId
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoPrivateKeyId, private_key_id))
	}
	if private_key == "" {
		private_key = dpmPrivateKey
		gdrive.display.Warning(wrnSetCredentialsContentNoPrivateKey)
	}
	if client_email == "" {
		client_email = dpmClientEmail
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoClientEmail, client_email))
	}
	if client_id == "" {
		client_id = dpmClientId
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoClientId, client_id))
	}
	if auth_uri == "" {
		auth_uri = dpmUriAuth
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoAuthUri, auth_uri))
	}
	if token_uri == "" {
		token_uri = dpmUriToken
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoTokenUri, token_uri))
	}
	if auth_provider_x509_cert_url == "" {
		auth_provider_x509_cert_url = dpmUriAuthProviderX509Cert
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoAuthProviderX509Cert, auth_provider_x509_cert_url))
	}
	if client_x509_cert_url == "" {
		client_x509_cert_url = dpmUriClientX509Cert
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoClientX509Cert, client_x509_cert_url))
	}
	if universe_domain == "" {
		universe_domain = dpmUniverseDomain
		gdrive.display.Warning(fmt.Sprintf(wrnSetCredentialsContentNoUniverseDomain, universe_domain))
	}
	credentials := &SACreds{
		Type:                typeName,
		ProjectID:           project_id,
		PrivateKeyID:        private_key_id,
		PrivateKey:          private_key,
		ClientEmail:         client_email,
		ClientID:            client_id,
		AuthURI:             auth_uri,
		TokenURI:            token_uri,
		AuthProviderCertURL: auth_provider_x509_cert_url,
		ClientCertURL:       client_x509_cert_url,
		UniverseDomain:      universe_domain,
	}

	var sb strings.Builder
	encoder := json.NewEncoder(&sb)
	encoder.SetEscapeHTML(false)
	if err := encoder.Encode(credentials); err != nil {
		fmt.Println("Error encoding JSON:", err)
		return err
	}
	jsonString := strings.ReplaceAll(sb.String(), "\\\\n", "\\n")

	tempFile, err := os.Create(gdrive.CredentialFilePath)
	if err != nil {
		return fmt.Errorf(errSetCredentialsContentFileCreateNok, gdrive.CredentialFilePath, err)
	}
	defer tempFile.Close()

	_, err = tempFile.WriteString(jsonString)
	if err != nil {
		return fmt.Errorf(errSetCredentialsContentFileWriteNok, gdrive.CredentialFilePath, err)
	}

	gdrive.display.Debug(fmt.Sprintf(dbgSetCredentialsContentOK, gdrive.CredentialFilePath))
	return nil
}

// CheckCredentials verifies if the credentials file is valid for connecting to Google API
func (gdrive *GDrive) CheckCredentials() error {
	ctx := context.Background()
	httpClient, _, errHttp := transport.NewHTTPClient(ctx, option.WithCredentialsFile(gdrive.CredentialFilePath))
	if errHttp != nil {
		return fmt.Errorf(errCheckCredentialsVerifReqCreate, errHttp)
	}
	req, err := http.NewRequest("GET", "https://www.googleapis.com/oauth2/v1/tokeninfo", nil)
	if err != nil {
		return fmt.Errorf(errCheckCredentialsVerifReqCreate, err)
	}
	res, err := httpClient.Do(req)
	if err != nil || res.StatusCode != 200 {
		return fmt.Errorf(errCheckCredentialsVerifReqNok, err)
	}
	gdrive.display.Debug(dbgCheckCredentialsVerifOK)
	return nil
}
