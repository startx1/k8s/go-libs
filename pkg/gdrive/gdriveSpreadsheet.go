package gdrive

import (
	"context"
	"errors"
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	drive "google.golang.org/api/drive/v3"
	"google.golang.org/api/option"
	sheets "google.golang.org/api/sheets/v4"
)

const (
	// GroupName is the group name use in this package
	GroupNameGDrive                    = "gsheet"
	dbgNewGDriveSpreadsheet            = "Initialize a GDriveSpreadsheet object"
	errSetSpreadsheetServiceNOK        = "unable to get spreadsheet because : %v"
	errSetSpreadsheetServiceNoCreds    = "unable to create spreadsheet service without gdrive credentials"
	dbgSetSpreadsheetServiceOK         = "Spreadsheet service created using %s"
	errAddSpreadsheetNOK               = "unable to add spreadsheet because : %v"
	errAddSpreadsheetEmpty             = "unable to add spreadsheet with an empty title"
	dbgAddSpreadsheetOK                = "Spreadsheet %s added"
	errGetSpreadsheetNOK               = "unable to get spreadsheet because : %v"
	errGetSpreadsheetEmpty             = "unable to load spreadsheet because spreadsheetID is empty"
	dbgGetSpreadsheetOK                = "Spreadsheet %s whas found"
	errLoadSpreadsheetNOK              = "unable to load spreadsheet because : %v"
	errLoadSpreadsheetEmpty            = "unable to load spreadsheet because spreadsheetID is empty"
	dbgLoadSpreadsheetOK               = "Spreadsheet %s loaded"
	errGetSheetByTitleNOK              = "unable to find sheet with title %s"
	errGetSheetByTitleEmpty            = "unable to find sheet with an empty title"
	dbgGetSheetByTitleOK               = "Sheet with title %s found in spreadsheet %s. SheetID is %s"
	errAddSheetNOK                     = "unable to add sheet because : %s"
	errAddSheetTitleEmpty              = "unable to add sheet with an empty title"
	dbgAddSheetOK                      = "Sheet with title %s added in spreadsheet %s. SheetID is %s"
	errAddSpreadsheetDriveNOK          = "unable to access drive because : %v"
	errAddSpreadsheetDriveNOPerm       = "unable to set permission: %v"
	dbgAddSpreadsheetDriveEnableDomain = "Permissions for spreadsheet %s set to %s organization access"
)

// GDrive is a wrapper around api.Config
type GDriveSpreadsheet struct {
	GDrive        *GDrive
	Service       *sheets.Service
	SpreadsheetID string
	Spreadsheet   *sheets.Spreadsheet
	display       *sxUtils.CmdDisplay
}

//
// Define constructor and public functions
//

// Initialize a gdrive object
// ex:
//
//	gdrive := kc.NewGDrive()
func NewGDriveSpreadsheet(gdrive *GDrive, spreadsheetID string) *GDriveSpreadsheet {
	return newGDriveSpreadsheet(gdrive, spreadsheetID)
}

//
// Define object internal methods
//

// internal used to construct a new GDriveSpreadsheet
func newGDriveSpreadsheet(gdrive *GDrive, spreadsheetID string) *GDriveSpreadsheet {
	display := sxUtils.NewCmdDisplay(GroupNameGDrive)
	s := GDriveSpreadsheet{
		GDrive:        gdrive,
		Service:       &sheets.Service{},
		SpreadsheetID: spreadsheetID,
		Spreadsheet:   &sheets.Spreadsheet{},
		display:       display,
	}
	return &s
}

// Initialize a spreadsheet must be used prior to calling any other method
func (spreadsheet *GDriveSpreadsheet) InitSpreadsheet() error {
	err := spreadsheet.SetSpreadsheetService()
	if err != nil {
		return err
	}
	if spreadsheet.SpreadsheetID != "" {
		spreadsheet.LoadSpreadsheet()
	}
	return nil
}

// Set the spreadsheet Service
func (spreadsheet *GDriveSpreadsheet) SetSpreadsheetService() error {
	if spreadsheet.GDrive.CredentialFilePath == "" {
		return errors.New(errSetSpreadsheetServiceNoCreds)
	}
	ctx := context.Background()
	service, err := sheets.NewService(ctx, option.WithCredentialsFile(spreadsheet.GDrive.CredentialFilePath))
	if err != nil {
		return fmt.Errorf(errSetSpreadsheetServiceNOK, err)
	}
	spreadsheet.display.Debug(fmt.Sprintf(dbgSetSpreadsheetServiceOK, spreadsheet.GDrive.CredentialFilePath))
	spreadsheet.Service = service
	return nil
}

// Add a spreadsheet and load it
func (spreadsheet *GDriveSpreadsheet) AddSpreadsheet(spreadsheetTitle string, isDomain bool) error {
	if spreadsheetTitle == "" {
		return errors.New(errAddSpreadsheetEmpty)
	}
	rb := &sheets.Spreadsheet{
		Properties: &sheets.SpreadsheetProperties{
			Title: spreadsheetTitle,
		},
	}
	spreadsheetNew, err := spreadsheet.Service.Spreadsheets.Create(rb).Context(context.Background()).Do()
	if err != nil {
		return fmt.Errorf(errAddSpreadsheetNOK, err)
	}
	spreadsheet.SpreadsheetID = spreadsheetNew.SpreadsheetId

	if isDomain {
		driveService, err := drive.NewService(context.Background(), option.WithCredentialsFile(spreadsheet.GDrive.CredentialFilePath))
		if err != nil {
			return fmt.Errorf(errAddSpreadsheetDriveNOK, err)
		}

		permission := &drive.Permission{
			Type:   "domain",
			Role:   "writer",
			Domain: spreadsheet.GDrive.Domain,
		}

		_, err = driveService.Permissions.Create(spreadsheet.SpreadsheetID, permission).Context(context.Background()).Do()
		if err != nil {
			return fmt.Errorf(errAddSpreadsheetDriveNOPerm, err)
		} else {
			spreadsheet.display.Debug(fmt.Sprintf(dbgAddSpreadsheetDriveEnableDomain, spreadsheet.SpreadsheetID, spreadsheet.GDrive.Domain))
		}
	}

	spreadsheet.display.Debug(fmt.Sprintf(dbgAddSpreadsheetOK, spreadsheet.SpreadsheetID))
	return spreadsheet.LoadSpreadsheet()
}

// Get a spreadsheet and return its object
func (spreadsheet *GDriveSpreadsheet) GetSpreadsheet(spreadsheetID string) (*sheets.Spreadsheet, error) {
	ctx := context.Background()
	defaultSpreadsheet := &sheets.Spreadsheet{}
	if spreadsheetID == "" {
		return defaultSpreadsheet, errors.New(errGetSpreadsheetEmpty)
	}
	spreadsheetFound, err := spreadsheet.Service.Spreadsheets.Get(spreadsheetID).Context(ctx).Do()
	if err != nil {
		return defaultSpreadsheet, fmt.Errorf(errGetSpreadsheetNOK, err)
	}
	spreadsheet.display.Debug(fmt.Sprintf(dbgGetSpreadsheetOK, spreadsheetID))
	return spreadsheetFound, nil
}

// Load a spreadsheet
func (spreadsheet *GDriveSpreadsheet) LoadSpreadsheet() error {
	if spreadsheet.SpreadsheetID == "" {
		return errors.New(errLoadSpreadsheetEmpty)
	}
	spreadsheetFound, err := spreadsheet.GetSpreadsheet(spreadsheet.SpreadsheetID)
	if err != nil {
		return fmt.Errorf(errLoadSpreadsheetNOK, err)
	}
	spreadsheet.Spreadsheet = spreadsheetFound
	spreadsheet.display.Debug(fmt.Sprintf(dbgLoadSpreadsheetOK, spreadsheet.SpreadsheetID))
	return nil
}

// Get a sheet and return its ID
func (spreadsheet *GDriveSpreadsheet) GetSheetByTitle(title string) (int64, error) {
	if title == "" {
		return 0, errors.New(errGetSheetByTitleEmpty)
	}
	var sheetID int64
	for _, sheet := range spreadsheet.Spreadsheet.Sheets {
		if sheet.Properties.Title == title {
			sheetID = sheet.Properties.SheetId
			break
		}
	}
	if sheetID == 0 {
		return sheetID, fmt.Errorf(errGetSheetByTitleNOK, title)
	} else {
		spreadsheet.display.Debug(fmt.Sprintf(dbgGetSheetByTitleOK, title, spreadsheet.SpreadsheetID, fmt.Sprint(sheetID)))
		return sheetID, nil
	}
}

// Add a sheet and return its object
func (spreadsheet *GDriveSpreadsheet) AddSheet(title string) (int64, error) {
	if title == "" {
		return 0, errors.New(errAddSheetTitleEmpty)
	}
	addSheetRequest := &sheets.Request{
		AddSheet: &sheets.AddSheetRequest{
			Properties: &sheets.SheetProperties{
				Title: title,
			},
		},
	}
	batchUpdateRequest := &sheets.BatchUpdateSpreadsheetRequest{
		Requests: []*sheets.Request{addSheetRequest},
	}
	_, err := spreadsheet.
		Service.
		Spreadsheets.
		BatchUpdate(
			spreadsheet.SpreadsheetID,
			batchUpdateRequest,
		).
		Context(
			context.Background(),
		).
		Do()
	if err != nil {
		return 0, fmt.Errorf(errAddSheetNOK, title)
	}
	sheetId := addSheetRequest.AddSheet.Properties.SheetId
	spreadsheet.display.Debug(fmt.Sprintf(dbgAddSheetOK, title, spreadsheet.SpreadsheetID, fmt.Sprint(sheetId)))
	return sheetId, nil
}

// Update or insert a sheet and return its object
func (spreadsheet *GDriveSpreadsheet) UpsertSheet(title string) (int64, error) {
	sheetID, err := spreadsheet.GetSheetByTitle(title)
	if err != nil {
		sheetID, err = spreadsheet.AddSheet(title)
	}
	return sheetID, err
}
