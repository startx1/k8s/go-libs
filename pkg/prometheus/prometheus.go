package prometheus

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	routev1 "github.com/openshift/api/route/v1" // Importer les types de route OpenShift
	promApi "github.com/prometheus/client_golang/api"
	promv1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme" // Importer le schéma du client Kubernetes
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName                                            = "prometheus"
	dbgInitnewPrometheus                                 = "Init Prometheus object"
	dbgNewPrometheus                                     = "Initialize a prometheus object"
	dbgStartLoadFromFile                                 = "Load prometheus from file %s"
	dbgStartLoadFromEnv                                  = "Load prometheus from the KUBECONFIG env var"
	dbgStartLoadFromHome                                 = "Load prometheus from the home directory"
	dbgStartLoadFromYaml                                 = "Load prometheus from a Yaml object"
	dbgStartLoadFromString                               = "Load prometheus from a YAML string"
	dbgStartLoad                                         = "Automatic search of an available prometheus"
	dbgStartSave                                         = "save the prometheus to the path"
	dbgStartConnect                                      = "Connect to the cluster"
	dbgStartDisconnect                                   = "Disconnect from the cluster"
	dbgKCfgStart                                         = "DEBUG the prometheus"
	dbgKCfgConfig                                        = "DEBUG - prometheus string val     : %s"
	dbgKCfgPath                                          = "DEBUG - prometheuspath string val : %s"
	errPHGetOCPPrometheusConnectionInfoK8ClientNOK       = "could not connect to Openshift because %v"
	errPHGetOCPPrometheusConnectionInfoOCPSchemeNOK      = "could not add route scheme because %v"
	errPHGetOCPPrometheusConnectionInfoNotOCP            = "no kind Route found. Probably not connected to an Openshift cluster"
	dbgExtractOCPPrometheusConnectionInfoPrometheusRoute = "Extracting OpenShift Prometheus route %s"
	errPHExtractOCPPrometheusConnectionSATokenSANOK      = "could not find the ServiceAccount %s because %v"
	errPHExtractOCPPrometheusConnectionSATokenNOK        = "could not get token %s in ServiceAccount %s because %v"
	errPHExtractOCPPrometheusConnectionSATokenSecretNOK  = "could not get a token secret in ServiceAccount %s because %v"
	dbgPHExtractOCPPrometheusConnectionSATokenOK         = "Token %s found (ServiceAccount %s found in namespace %s)"
	dbgPHPreparePrometheusConnection                     = "Start preparing Prometheus connection to %s"
	errPHPreparePrometheusConnectionClientNOK            = "error preparing connection client because %v"
	dbgPHExecPrometheusQuery                             = "Start executing Prometheus request against %s"
	dbgPHGetExecPrometheusQueryHttpPrepareUriOK          = "Start preparing Prometheus URI agaisnt %s"
	dbgPHGetExecPrometheusQueryHttpPrepareHttpOK         = "Start preparing Prometheus Http request for query %s"
	dbgPHGetExecPrometheusQueryHttpPrepareHttpClientOK   = "Start preparing Prometheus HttpClient for query %s"
	errPHGetExecPrometheusQueryHttpStartNOK              = "could not start Prometheus request to %s because %v"
	errPHGetExecPrometheusQueryHttpParseNOK              = "could not parse Prometheus request because %v"
	errPHGetExecPrometheusQueryHttpExecNOK               = "could not execute Prometheus request %s because %v"
	errPHGetExecPrometheusQueryHttpDecodeNOK             = "could not decode Prometheus response because %v"
	errPHGetExecPrometheusQueryHttpResponseNOK           = "wrong Prometheus response because %v"
	errPHGetExecPrometheusQueryHttpPrepareUriNOK         = "could not prepare URI %s for Prometheus request because %v"
	dbgPHExecPrometheusMultipleQuery                     = "Start executing multiple Prometheus requests"
	dbgPHExecPrometheusMultipleQueryResultWarnings       = "Prometheus warnings : %v"
	dbgPHExecPrometheusMultipleQueryRequest              = "Request Prometheus data for query %s. request is : %s"
	dbgPHExecPrometheusMultipleQueryRequestResult        = "Result  Prometheus data for query %s. result count is : %d"
	errPluginPrometheusParse                             = "error parsing Prometheus response"
)

type PrometheusResponse struct {
	Status string `json:"status"`
	Data   struct {
		ResultType string `json:"resultType"`
		Result     []struct {
			Metric struct {
				Instance string `json:"instance"`
				Node     string `json:"node"`
			} `json:"metric"`
			Value []interface{} `json:"value"`
		} `json:"result"`
	} `json:"data"`
}

// customTransport is a struct that embeds the base transport and includes the token
type customTransport struct {
	transport http.RoundTripper
	token     string
}

// RoundTrip is the implementation that injects the Bearer token into each request
func (ct *customTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	// Add the Authorization header with Bearer token
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", ct.token))

	// Proceed with the default RoundTripper
	return ct.transport.RoundTrip(req)
}

//
// Define object variables
//

// Prometheus structure
type Prometheus struct {
	URL     string
	Token   string
	kclient *sxKCli.K8sClient
	display *sxUtils.CmdDisplay
}

//
// Define constructor and public functions
//

// Initialize a kubeconfig object
// ex:
//
//	kubeconfig := kc.NewPrometheus()
func NewPrometheus(kclient *sxKCli.K8sClient) *Prometheus {
	// sxUtils.DisplayDebug(GroupName,dbgNewPrometheus)
	return newPrometheus(kclient)
}

//
// Define object internal methods
//

// internal used to construct a new api.URL
func newPrometheus(kclient *sxKCli.K8sClient) *Prometheus {
	// sxUtils.DisplayDebug(GroupName,dbgInitnewPrometheus)
	return &Prometheus{
		URL:     "",
		Token:   "",
		kclient: kclient,
		display: sxUtils.NewCmdDisplay(GroupName),
	}
}

// Find connection details about OpenShift Prometheus
func (prom *Prometheus) SetPrometheusOCP() error {
	display := sxUtils.NewCmdDisplay(GroupName)
	namespace := "openshift-monitoring"
	routeName := "prometheus-k8s"
	saName := "prometheus-k8s"
	saToken := ""

	// find the prometheus URL
	prometheusURL, errUrl := prom.FindPrometheusOCPRoute(namespace, routeName)
	if errUrl != nil {
		return errUrl
	}

	// find the kubeconfig token
	tokenK, errKT := prom.kclient.GetKubeconfigToken()
	if errKT != nil {
		display.Warning(errKT.Error())
		// find the kubeconfig token
		tokenUrl, errUrl := prom.FindPrometheusOCPSAToken(namespace, saName)
		if errUrl != nil {
			return errUrl
		}
		saToken = tokenUrl
	} else {
		saToken = tokenK
	}

	prom.URL = prometheusURL
	prom.Token = saToken
	return nil
}

// Get the prometheus route URL
func (prom *Prometheus) FindPrometheusOCPRoute(namespaceIn string, routeIn string) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupName)
	// define namespace holding the ServiceAccount
	namespace := "openshift-monitoring"
	if namespaceIn != "" {
		namespace = namespaceIn
	}
	// define the name of the route to find
	routeName := "prometheus-k8s"
	if routeIn != "" {
		routeName = routeIn
	}
	// create the k8s client
	client, err := client.New(prom.kclient.GetConfig(), client.Options{})
	if err != nil {
		display.Error(fmt.Sprintf(errPHGetOCPPrometheusConnectionInfoK8ClientNOK, err))
		return "", err
	}
	// Add route scheme to k8s client
	err = routev1.AddToScheme(scheme.Scheme)
	if err != nil {
		display.Error(fmt.Sprintf(errPHGetOCPPrometheusConnectionInfoOCPSchemeNOK, err))
		return "", err
	}
	// Search for the route
	route := &routev1.Route{}
	errRoute := client.Get(context.TODO(), types.NamespacedName{
		Namespace: namespace,
		Name:      routeName,
	}, route)
	if errRoute != nil {
		if strings.Contains(errRoute.Error(), "no matches for kind") {
			errRoute = errors.New(errPHGetOCPPrometheusConnectionInfoNotOCP)
		}
		return "", errRoute
	}
	prometheusURL := "https://" + route.Spec.Host
	display.Debug(fmt.Sprintf(dbgExtractOCPPrometheusConnectionInfoPrometheusRoute, prometheusURL))
	return prometheusURL, nil
}

// Get the prometheus SA token located into the given namespace and according to the given ServiceAccount name
func (prom *Prometheus) FindPrometheusOCPSAToken(namespaceIn string, saIn string) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupName)
	// define namespace holding the ServiceAccount
	namespace := "openshift-monitoring"
	if namespaceIn != "" {
		namespace = namespaceIn
	}
	// define the name of the ServiceAccount
	saName := "prometheus-k8s"
	if saIn != "" {
		saName = saIn
	}

	// Search for the ServiceAccount in the namespace
	serviceAccount, err := prom.kclient.Clientset.CoreV1().ServiceAccounts(namespace).Get(context.TODO(), saName, metav1.GetOptions{})
	if err != nil {
		display.Error(fmt.Sprintf(errPHExtractOCPPrometheusConnectionSATokenSANOK, saName, err))
		return "", err
	}
	var tokenSecretName string
	// Search the name of the token secret associated to the ServiceAccount
	for _, secret := range serviceAccount.Secrets {
		if strings.Contains(secret.Name, "token") {
			tokenSecretName = secret.Name
			break
		}
	}
	if tokenSecretName == "" {
		msgErr := fmt.Sprintf(errPHExtractOCPPrometheusConnectionSATokenNOK, saName, namespace, err)
		display.Error(msgErr)
		return "", errors.New(msgErr)
	}
	// Search for the secret corresponding to the token of the ServiceAccount
	secret, err := prom.kclient.Clientset.CoreV1().Secrets(namespace).Get(context.TODO(), tokenSecretName, metav1.GetOptions{})
	if err != nil {
		display.Error(fmt.Sprintf(errPHExtractOCPPrometheusConnectionSATokenSecretNOK, tokenSecretName, err))

		return "", err
	}
	token := secret.Data["token"]
	display.Debug(fmt.Sprintf(dbgPHExtractOCPPrometheusConnectionSATokenOK, token, saName, namespace))
	return string(token), nil
}

// Prepare the connection to Prometheus
func (prom *Prometheus) GetPrometheusConnection(insecure bool) (promv1.API, error) {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug(fmt.Sprintf(dbgPHPreparePrometheusConnection, prom.URL))
	// Create a custom HTTP transport
	transport := &http.Transport{}
	if insecure {
		transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		}
	}

	// Create a custom HTTP client that uses the transport
	client := &http.Client{
		Transport: transport,
	}
	// Create a custom RoundTripper by setting a middleware-like function that injects the token
	client.Transport = &customTransport{
		transport: transport,
		token:     prom.Token,
	}
	// Création d'un client API pour Prometheus
	promcli, err := promApi.NewClient(promApi.Config{
		Address: prom.URL,
		Client:  client,
	})
	if err != nil {
		display.Error(fmt.Sprintf(errPHPreparePrometheusConnectionClientNOK, err))
		return nil, err
	}

	// Créez une nouvelle API v1 pour interroger Prometheus
	v1api := promv1.NewAPI(promcli)

	return v1api, nil
}

// Find informations about OpenShift Prometheus
func (prom *Prometheus) ExecPrometheusQuery(promQuery string) ([][]string, error) {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug(fmt.Sprintf(dbgPHExecPrometheusQuery, prom.URL))
	result := [][]string{}
	reqUrl, err := url.Parse(prom.URL)
	if err != nil {
		display.Error(fmt.Sprintf(errPHGetExecPrometheusQueryHttpPrepareUriNOK, prom.URL, err))
		return result, err
	} else {
		display.Debug(fmt.Sprintf(dbgPHGetExecPrometheusQueryHttpPrepareUriOK, promQuery))
	}
	queryParams := reqUrl.Query()
	queryParams.Set("query", promQuery)
	reqUrl.RawQuery = queryParams.Encode()
	req, err := http.NewRequest("GET", reqUrl.String(), nil)
	if err != nil {
		display.Error(fmt.Sprintf(errPHGetExecPrometheusQueryHttpStartNOK, reqUrl.String(), err))
		return result, err
	} else {
		display.Debug(fmt.Sprintf(dbgPHGetExecPrometheusQueryHttpPrepareHttpOK, promQuery))
	}
	if prom.Token != "" {
		req.Header.Set("Authorization", "Bearer "+prom.Token)
	}
	clientHTTP := &http.Client{}
	resp, err := clientHTTP.Do(req)
	if err != nil {
		display.Error(fmt.Sprintf(errPHGetExecPrometheusQueryHttpExecNOK, prom.URL, err))
		return result, err
	} else {
		display.Debug(fmt.Sprintf(dbgPHGetExecPrometheusQueryHttpPrepareHttpClientOK, promQuery))
	}
	defer resp.Body.Close()
	var promResponse PrometheusResponse
	err = json.NewDecoder(resp.Body).Decode(&promResponse)
	if err != nil {
		display.Error(fmt.Sprintf(errPHGetExecPrometheusQueryHttpDecodeNOK, err))
		return result, err
	}
	if promResponse.Status != "success" {
		msgErr := fmt.Sprintf(errPHGetExecPrometheusQueryHttpResponseNOK, promResponse)
		display.Error(msgErr)
		return result, errors.New(msgErr)
	}
	for _, line := range promResponse.Data.Result {
		// display.Debug(fmt.Sprintf("Result line: %v", line))
		instance := line.Metric.Instance
		node := line.Metric.Node
		if instance == "" {
			instance = node
		}
		value := fmt.Sprint(line.Value[1])
		row := []string{
			instance,
			value,
		}
		result = append(result, row)
	}
	return result, nil
}

func (prom *Prometheus) ExecPrometheusMultipleQuery(queries map[string]string, promApi promv1.API, startDate time.Time, stopDate time.Time, stepTime time.Duration, metricKey string, metricKey2 string, deep bool) (map[string]map[string]map[string]map[string]float64, error) {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug(dbgPHExecPrometheusMultipleQuery)
	deepDebug := false
	if deep {
		deepDebug = true
	}
	outputData := make(map[string]map[string]map[string]map[string]float64)

	// Contexte pour annuler la requête si besoin
	ctx, cancel := context.WithTimeout(context.Background(), 180*time.Second)
	defer cancel()

	// Parcourir les requêtes et exécuter la demande sur Prometheus
	for queryName, query := range queries {
		display.Debug(fmt.Sprintf(dbgPHExecPrometheusMultipleQueryRequest, queryName, query))

		// Exécution de la requête pour la période d'un mois
		result, warnings, err := promApi.QueryRange(ctx, query, promv1.Range{
			Start: startDate,
			End:   stopDate,
			Step:  stepTime,
		})
		if err != nil {
			return outputData, err
		}

		if len(warnings) > 0 {
			display.Warning(fmt.Sprintf(dbgPHExecPrometheusMultipleQueryResultWarnings, warnings))
		}

		vector, ok := result.(model.Matrix)
		if !ok {
			return outputData, errors.New(errPluginPrometheusParse)
		}
		outputData[queryName] = make(map[string]map[string]map[string]float64, len(vector))

		display.Debug(fmt.Sprintf(dbgPHExecPrometheusMultipleQueryRequestResult, queryName, len(vector)))
		for _, sample := range vector {
			key,
				key2,
				valueLow,
				valueHigh,
				valueFirst,
				valueLast,
				valueSum,
				valueAvg,
				valueCount,
				errParse := prom.QueryParseModelMetric(queryName, sample, metricKey, metricKey2)
			if errParse != nil {
				return outputData, errParse
			}
			if deepDebug {
				display.Debug(fmt.Sprintf("-------------- : %s %s", key, key2))
				display.Debug(fmt.Sprintf("- values count : %d", valueCount))
				display.Debug(fmt.Sprintf("---   valueLow : %f", valueLow))
				display.Debug(fmt.Sprintf("---  valueHigh : %f", valueHigh))
				display.Debug(fmt.Sprintf("--- valueFirst : %f", valueFirst))
				display.Debug(fmt.Sprintf("---  valueLast : %f", valueLast))
				display.Debug(fmt.Sprintf("---   valueSum : %f", valueSum))
				display.Debug(fmt.Sprintf("---   valueAvg : %f", valueAvg))
			}
			if _, exists := outputData[queryName][key]; !exists {
				outputData[queryName][key] = make(map[string]map[string]float64, 7)
			}
			if _, exists := outputData[queryName][key][key2]; !exists {
				outputData[queryName][key][key2] = make(map[string]float64, 7)
			}
			outputData[queryName][key][key2]["valueCount"] = float64(valueCount)
			outputData[queryName][key][key2]["valueLow"] = valueLow
			outputData[queryName][key][key2]["valueHigh"] = valueHigh
			outputData[queryName][key][key2]["valueFirst"] = valueFirst
			outputData[queryName][key][key2]["valueLast"] = valueLast
			outputData[queryName][key][key2]["valueSum"] = valueSum
			outputData[queryName][key][key2]["valueAvg"] = valueAvg
		}
	}
	return outputData, nil
}

func (prom *Prometheus) QueryParseModelMetric(queryName string, sample *model.SampleStream, metricKey string, metricKey2 string) (string, string, float64, float64, float64, float64, float64, float64, int, error) {
	key := string(sample.Metric[model.LabelName(metricKey)])
	key2 := ""
	if metricKey2 != "" {
		key2 = string(sample.Metric[model.LabelName(metricKey2)])
	}
	firstVal := float64(sample.Values[0].Value)
	valueCount := len(sample.Values)
	valueLow := firstVal
	valueHigh := firstVal
	valueFirst := firstVal
	valueLast := firstVal
	valueSum := float64(0)
	valueAvg := float64(0)
	firstTmstp := 0
	lastTmstp := 0

	for _, y := range sample.Values {
		if float64(y.Value) > valueHigh {
			valueHigh = float64(y.Value)
		}
		if float64(y.Value) < valueLow {
			valueLow = float64(y.Value)
		}
		if int(y.Timestamp) > lastTmstp {
			valueLast = float64(y.Value)
			lastTmstp = int(y.Timestamp)
		}
		if int(y.Timestamp) < firstTmstp {
			valueFirst = float64(y.Value)
			firstTmstp = int(y.Timestamp)
		}
		valueSum = valueSum + float64(y.Value)
	}
	valueAvg = valueSum / float64(len(sample.Values))
	return key, key2, valueLow, valueHigh, valueFirst, valueLast, valueSum, valueAvg, valueCount, nil
}
