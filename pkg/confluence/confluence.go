package confluence

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName                       = "confluence"
	dbgNewConfluence                = "Initialize a confluence object for %s with user %s"
	dbgCreateBegin                  = "Begin creating page %s"
	dbgCreateEnd                    = "End creating page %s"
	dbgUpdateBegin                  = "Begin updating page %s"
	errUpdatePageNotFound           = "pageID %s not found (http status: %s). Update required to have ether a --confluence-pageid or CONFLUENCE_PAGE_ID set with a valid PageID"
	dbgUpdateEnd                    = "End updating page %s"
	errUpdatePageMissingPageID      = "pageID is required for page update operation"
	dbgAppendBegin                  = "Begin appending page %s"
	errAppendPageNotFound           = "pageID %s not found (http status: %s). Append required to have ether a --confluence-pageid or CONFLUENCE_PAGE_ID set with a valid PageID"
	errAppendPageMissingPageID      = "pageID is required for page append operation"
	dbgAppendEnd                    = "End appending page %s"
	dbgCreateOrAppendBegin          = "Begin creating or appending page %s"
	dbgCreateOrAppendInacessible    = "error appending to page %s because status: %s"
	dbgCreateOrAppendEnd            = "End creating or appending page %s"
	wrnConfluenceSendNOK            = "Could not send email to %s : %v"
	dbgExtractPageResponseBegin     = "Begin extracting page response"
	dbgExtractPageResponseEnd       = "End extracting page response"
	errExtractPageResponseNotJson   = "error page response is not a JSON response"
	errExtractPageResponseParseJson = "error page response parsing the JSON response because %v"
	dbgExtractPageVersionBegin      = "Begin extracting page version from page response"
	errExtractPageVersionNoVersion  = "error unable to extract version from page content"
	dbgExtractPageVersionEnd        = "End extracting page version from page response"
	dbgExtractPageContentBegin      = "Begin extracting page content from page response"
	errExtractPageContentNoBody     = "error unable to extract body from page content"
	errExtractPageContentNoStorage  = "error unable to extract body.storage from page content"
	errExtractPageContentNoValue    = "error unable to extract body.storage.value from page content"
	dbgExtractPageContentEnd        = "End extracting page content from page response"
	dbgExtractPageMessageBegin      = "Begin extracting page message from page response"
	errExtractPageMessageNoMessage  = "error unable to extract message from page content"
	dbgExtractPageMessageEnd        = "End extracting page message from page response"
	dbg_GETBegin                    = "Begin GET url %s"
	dbg_GETEnd                      = "End GET url %s status %s"
	dbg_POSTBegin                   = "Begin POST url %s"
	dbg_POSTEnd                     = "End POST url %s status %s"
	dbg_PUTBegin                    = "Begin PUT url %s"
	dbg_PUTEnd                      = "End PUT url %s status %s"
	dbg_DELETEBegin                 = "Begin DELETE url %s"
	dbg_DELETEEnd                   = "End DELETE url %s status %s"
)

//
// Define object variables
//

// Confluence is a wrapper around api.Config
type Confluence struct {
	BaseURL      string
	Username     string
	ApiToken     string
	SpaceKey     string
	ParentPageID string
	display      *sxUtils.CmdDisplay
}

//
// Define constructor and public functions
//

// Initialize a confluence object
// ex:
//
//	confluence := kc.NewConfluence()
func NewConfluence(baseURL string, username string, apiToken string, spaceKey string) *Confluence {
	return newConfluence(baseURL, username, apiToken, spaceKey)
}

// internal used to construct a new api.Config
func newConfluence(baseURL string, username string, apiToken string, spaceKey string) *Confluence {
	display := sxUtils.NewCmdDisplay(GroupName)
	if baseURL == "" {
		baseURL = "https://your-domain.atlassian.net/wiki"
	}
	if username == "" {
		username = "myaccount@example.com"
	}
	if apiToken == "" {
		apiToken = "my_confluent_token"
	}
	if spaceKey == "" {
		spaceKey = "your confluent spacekey"
	}
	display.Debug(fmt.Sprintf(dbgNewConfluence, baseURL, username))
	return &Confluence{
		BaseURL:  baseURL,
		Username: username,
		ApiToken: apiToken,
		display:  display,
	}
}

// Extract the page response from the body of a confluence request
func (confluence *Confluence) ExtractPageResponse(response *http.Response, body string) (map[string]interface{}, error) {
	var pageData map[string]interface{}
	confluence.display.Debug(dbgExtractPageResponseBegin)
	contentType := response.Header.Get("Content-Type")
	if contentType != "application/json" {
		return pageData, errors.New(errExtractPageResponseNotJson)
	}
	if err := json.Unmarshal([]byte(body), &pageData); err != nil {
		return pageData, fmt.Errorf(errExtractPageResponseParseJson, err)
	}
	confluence.display.Debug(dbgExtractPageResponseEnd)
	return pageData, nil
}

// Extract the page version from the body of a confluence request
func (confluence *Confluence) ExtractPageVersion(response *http.Response, body string) (int, error) {
	confluence.display.Debug(dbgExtractPageVersionBegin)
	currentVersion := 0
	pageData, err := confluence.ExtractPageResponse(response, body)
	if err != nil {
		return currentVersion, err
	}
	versionData, ok := pageData["version"].(map[string]interface{})
	if !ok {
		return currentVersion, errors.New(errExtractPageVersionNoVersion)
	}
	currentVersion = int(versionData["number"].(float64))
	confluence.display.Debug(dbgExtractPageVersionEnd)
	return currentVersion, nil
}

// Extract the page content from the body of a confluence request
func (confluence *Confluence) ExtractPageContent(response *http.Response, body string) (string, error) {
	confluence.display.Debug(dbgExtractPageContentBegin)
	currentContent := ""
	pageData, err := confluence.ExtractPageResponse(response, body)
	if err != nil {
		return currentContent, err
	}
	// Extract the current content
	bodyRaw, ok := pageData["body"].(map[string]interface{})
	if !ok {
		return currentContent, errors.New(errExtractPageContentNoBody)
	}
	storage, ok := bodyRaw["storage"].(map[string]interface{})
	if !ok {
		return currentContent, errors.New(errExtractPageContentNoStorage)
	}
	existingContent, ok := storage["value"].(string)
	if !ok {
		return currentContent, errors.New(errExtractPageContentNoValue)
	} else {
		currentContent = existingContent
	}
	confluence.display.Debug(dbgExtractPageContentEnd)
	return currentContent, nil
}

// Extract the page content from the body of a confluence request
func (confluence *Confluence) ExtractPageMessage(response *http.Response, body string) (string, error) {
	confluence.display.Debug(dbgExtractPageMessageBegin)
	currentMessage := ""
	pageData, err := confluence.ExtractPageResponse(response, body)
	if err != nil {
		return currentMessage, err
	}
	// Extract the current message
	existingMessage, ok := pageData["message"].(string)
	if !ok {
		return currentMessage, errors.New(errExtractPageMessageNoMessage)
	} else {
		currentMessage = existingMessage
	}
	confluence.display.Debug(dbgExtractPageMessageEnd)
	return currentMessage, nil
}

// Create a new GET page
func (confluence *Confluence) _GET(url string) (*http.Response, string, *http.Request, error) {
	confluence.display.Debug(fmt.Sprintf(dbg_GETBegin, url))
	body := ""
	req, err := http.NewRequest("GET", url, nil)
	resp := &http.Response{}
	if err != nil {
		return resp, body, req, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(confluence.Username, confluence.ApiToken)
	client := &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		return resp, body, req, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		return resp, body, req, fmt.Errorf("failed to request %s. Status: %s\nResponse: %s", url, resp.Status, resp.Body)
	}
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return resp, body, req, fmt.Errorf("error extracting body for url %s : %v", url, err)
	}
	body = string(bodyBytes)
	if body == "" {
		return resp, body, req, errors.New("error extracting page information because body is empty")
	}
	confluence.display.Debug(fmt.Sprintf(dbg_GETEnd, url, strconv.Itoa(resp.StatusCode)))
	return resp, body, req, nil
}

// Create a new POST page
func (confluence *Confluence) _POST(url string, payloadBytes []byte) (*http.Response, string, *http.Request, error) {
	confluence.display.Debug(fmt.Sprintf(dbg_POSTBegin, url))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payloadBytes))
	body := ""
	resp := &http.Response{}
	if err != nil {
		return resp, body, req, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(confluence.Username, confluence.ApiToken)
	client := &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		return resp, body, req, err
	}
	defer resp.Body.Close()
	bodyBytes, _ := io.ReadAll(resp.Body)
	body = string(bodyBytes)
	if resp.StatusCode != http.StatusOK {
		return resp, body, req, fmt.Errorf("failed to update page. Status: %s, content: %s", resp.Status, string(bodyBytes))
	}
	confluence.display.Debug(fmt.Sprintf(dbg_POSTEnd, url, strconv.Itoa(resp.StatusCode)))
	return resp, body, req, nil
}

// Create a new PUT page
func (confluence *Confluence) _PUT(url string, payloadBytes []byte) (*http.Response, string, *http.Request, error) {
	confluence.display.Debug(fmt.Sprintf(dbg_PUTBegin, url))
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(payloadBytes))
	body := ""
	resp := &http.Response{}
	if err != nil {
		return resp, body, req, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(confluence.Username, confluence.ApiToken)
	client := &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		return resp, body, req, err
	}
	defer resp.Body.Close()
	bodyBytes, _ := io.ReadAll(resp.Body)
	body = string(bodyBytes)
	if resp.StatusCode != http.StatusOK {
		return resp, body, req, fmt.Errorf("failed to update page. Status: %s, content: %s", resp.Status, string(bodyBytes))
	}
	confluence.display.Debug(fmt.Sprintf(dbg_PUTEnd, url, strconv.Itoa(resp.StatusCode)))
	return resp, body, req, nil
}

// Create a new DELETE page
func (confluence *Confluence) _DELETE(url string) (*http.Response, string, *http.Request, error) {
	confluence.display.Debug(fmt.Sprintf(dbg_DELETEBegin, url))
	body := ""
	req, err := http.NewRequest("DELETE", url, nil)
	resp := &http.Response{}
	if err != nil {
		return resp, body, req, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(confluence.Username, confluence.ApiToken)
	client := &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		return resp, body, req, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return resp, body, req, fmt.Errorf("failed to request %s. Status: %s\nResponse: %s", url, resp.Status, resp.Body)
	}
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return resp, body, req, fmt.Errorf("error extracting body for url %s : %v", url, err)
	}
	body = string(bodyBytes)
	if body == "" {
		return resp, body, req, errors.New("error extracting page information because body is empty")
	}
	confluence.display.Debug(fmt.Sprintf(dbg_DELETEEnd, url, strconv.Itoa(resp.StatusCode)))
	return resp, body, req, nil
}

// Create a new page
func (confluence *Confluence) CreatePage(page *ConfluencePage) (*http.Response, error) {
	confluence.display.Debug(fmt.Sprintf(dbgCreateBegin, page.Title))
	defaultHttpResponse := &http.Response{}
	url := fmt.Sprintf("%s/rest/api/content", confluence.BaseURL)
	pagePayload, err := page.GetNewPayload()
	if err != nil {
		return defaultHttpResponse, err
	}
	response, _, _, err := confluence._POST(url, pagePayload)
	if err != nil {
		return response, err
	}
	confluence.display.Debug(fmt.Sprintf(dbgCreateEnd, page.Title))
	return response, nil
}

// Update an existing page
func (confluence *Confluence) UpdatePage(page *ConfluencePage) (*http.Response, error) {
	confluence.display.Debug(fmt.Sprintf(dbgUpdateBegin, page.PageID))
	if page.PageID == "" {
		return nil, errors.New(errUpdatePageMissingPageID)
	}
	url := fmt.Sprintf("%s/rest/api/content/%s?expand=body.storage,version", confluence.BaseURL, page.PageID)
	pageResponse, bodyGet, _, err := confluence._GET(url)
	if err != nil {
		if pageResponse.StatusCode == http.StatusNotFound {
			return pageResponse, fmt.Errorf(errUpdatePageNotFound, page.PageID, pageResponse.Status)
		} else {
			return pageResponse, err
		}
	}
	currentVersion, err := confluence.ExtractPageVersion(pageResponse, bodyGet)
	if err != nil {
		return pageResponse, fmt.Errorf("error extracting page version: %v", err)
	}
	newVersion := currentVersion + 1
	updatePayloadBytes, err := page.GetUpdatePayload(newVersion)
	if err != nil {
		return pageResponse, err
	}
	urlUpdate := fmt.Sprintf("%s/rest/api/content/%s", confluence.BaseURL, page.PageID)
	updateResponse, _, _, err := confluence._PUT(urlUpdate, updatePayloadBytes)
	if err != nil {
		return updateResponse, err
	}
	confluence.display.Debug(fmt.Sprintf(dbgUpdateEnd, page.PageID))
	return updateResponse, nil
}

// Append content to an existing page
func (confluence *Confluence) AppendPage(page *ConfluencePage, sep string) (*http.Response, error) {
	confluence.display.Debug(fmt.Sprintf(dbgAppendBegin, page.PageID))
	if page.PageID == "" {
		return nil, errors.New(errAppendPageMissingPageID)
	}
	url := fmt.Sprintf("%s/rest/api/content/%s?expand=body.storage,version", confluence.BaseURL, page.PageID)
	pageResponse, bodyGet, _, err := confluence._GET(url)
	if err != nil {
		if pageResponse.StatusCode == http.StatusNotFound {
			return pageResponse, fmt.Errorf(errAppendPageNotFound, page.PageID, pageResponse.Status)
		} else {
			return pageResponse, err
		}
	}
	currentBody, err := confluence.ExtractPageContent(pageResponse, bodyGet)
	if err != nil {
		return pageResponse, fmt.Errorf("error extracting page content: %v", err)
	}
	currentVersion, err := confluence.ExtractPageVersion(pageResponse, bodyGet)
	if err != nil {
		return pageResponse, fmt.Errorf("error extracting page version: %v", err)
	}

	newVersion := currentVersion + 1
	page.HtmlContent = currentBody + sep + page.HtmlContent
	updatePayloadBytes, err := page.GetUpdatePayload(newVersion)
	if err != nil {
		return pageResponse, err
	}
	urlUpdate := fmt.Sprintf("%s/rest/api/content/%s", confluence.BaseURL, page.PageID)
	updateResponse, _, _, err := confluence._PUT(urlUpdate, updatePayloadBytes)
	if err != nil {
		return updateResponse, err
	}
	confluence.display.Debug(fmt.Sprintf(dbgAppendEnd, page.PageID))
	return updateResponse, nil
}

// Append content to an existing page
func (confluence *Confluence) CreateOrAppendPage(page *ConfluencePage, sep string) (*http.Response, error) {
	confluence.display.Debug(fmt.Sprintf(dbgCreateOrAppendBegin, page.PageID))
	mustCreate := false
	mustUpdate := false
	bodyGet := ""
	currentMessage := ""
	pageResponse := &http.Response{}
	response := &http.Response{}
	isOK := false
	err := errors.New("")

	if page.PageID != "" {
		url := fmt.Sprintf("%s/rest/api/content/%s?expand=body.storage,version", confluence.BaseURL, page.PageID)
		pageResponse, bodyGet, _, err = confluence._GET(url)
		if err != nil {
			if pageResponse.StatusCode == http.StatusNotFound {
				mustCreate = true
			} else {
				return pageResponse, fmt.Errorf(dbgCreateOrAppendInacessible, page.PageID, pageResponse.Status)
			}
		} else {
			mustUpdate = true
			response = pageResponse
		}
	} else {
		mustCreate = true
	}

	if mustCreate {
		response, err = confluence.CreatePage(page)
		if err != nil {
			currentMessage, err = confluence.ExtractPageMessage(response, bodyGet)
			if err != nil {
				return pageResponse, fmt.Errorf("error extracting page content: %v", err)
			} else {
				if strings.Contains(currentMessage, "exists with the same TITLE") {
					mustUpdate = true
					page.Title = page.Title + " - " + fmt.Sprint(time.Now().Unix())
				}
			}
			return response, err
		} else {
			isOK = true
		}
	}
	if mustUpdate {
		response, err := confluence.AppendPage(page, sep)
		if err != nil {
			return response, err
		} else {
			isOK = true
		}
	}
	confluence.display.Debug(fmt.Sprintf(dbgCreateOrAppendEnd, page.PageID))
	if isOK {
		return response, nil
	} else {
		return response, err
	}
}

// Delete a page
func (confluence *Confluence) DeletePage(page *ConfluencePage) (bool, error) {
	confluence.display.Debug(fmt.Sprintf(dbgCreateBegin, page.PageID))
	responseCode := false
	url := fmt.Sprintf("%s/rest/api/content", confluence.BaseURL)
	_, _, _, err := confluence._DELETE(url)
	if err != nil {
		return responseCode, err
	}
	responseCode = true
	confluence.display.Debug(fmt.Sprintf(dbgCreateEnd, page.PageID))
	return responseCode, nil
}
