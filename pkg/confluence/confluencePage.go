package confluence

import (
	"encoding/json"
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

const (
	// GroupName is the group name use in this package
	GroupNameConfluence                    = "confluencePage"
	dbgNewConfluencePage                   = "Initialize a ConfluencePage object in spacekey %s titled %s"
	dbgConfluencePageGetNewPayloadBegin    = "Begin generating a create payload for page %s in spacekey %s"
	dbgConfluencePageGetNewPayloadEnd      = "End generating a create payload for page %s in spacekey %s"
	dbgConfluencePageGetUpdatePayloadBegin = "Begin generating an update payload for page %s in spacekey %s"
	dbgConfluencePageGetUpdatePayloadEnd   = "End generating an update payload for page %s in spacekey %s"
)

// Confluence is a wrapper around api.Config
type ConfluencePage struct {
	SpaceKey     string
	PageID       string
	ParentPageID string
	Title        string
	HtmlContent  string
	display      *sxUtils.CmdDisplay
}

//
// Define constructor and public functions
//

// Initialize a confluence object
// ex:
//
//	confluence := kc.NewConfluence()
func NewConfluencePage(spaceKey string, pageID string, parentPageID string, title string, htmlContent string) *ConfluencePage {
	return newConfluencePage(spaceKey, pageID, parentPageID, title, htmlContent)
}

//
// Define object internal methods
//

// internal used to construct a new ConfluencePage
func newConfluencePage(spaceKey string, pageID string, parentPageID string, title string, htmlContent string) *ConfluencePage {
	display := sxUtils.NewCmdDisplay(GroupNameConfluence)
	page := ConfluencePage{
		SpaceKey:     spaceKey,
		PageID:       pageID,
		ParentPageID: parentPageID,
		Title:        title,
		HtmlContent:  htmlContent,
		display:      sxUtils.NewCmdDisplay(GroupNameConfluence),
	}
	display.Debug(fmt.Sprintf(dbgNewConfluencePage, spaceKey, title))
	return &page
}

// Record the confluence page
func (page *ConfluencePage) GetNewPayload() ([]byte, error) {
	page.display.Debug(fmt.Sprintf(dbgConfluencePageGetNewPayloadBegin, page.Title, page.SpaceKey))
	defaultPayloadBytes := []byte{}
	// Create the request payload
	payload := map[string]interface{}{
		"type": "page",
		"body": map[string]interface{}{
			"storage": map[string]string{
				"value":          page.HtmlContent,
				"representation": "storage",
			},
		},
	}
	// Include  page ID if specified
	if page.Title != "" {
		payload["title"] = page.Title
	}

	// Include parent page if specified
	if page.ParentPageID != "" {
		payload["ancestors"] = []map[string]string{
			{"id": page.ParentPageID},
		}
	}
	// Include parent page if specified
	if page.SpaceKey != "" {
		payload["space"] = map[string]string{
			"key": page.SpaceKey,
		}
	}

	// Convert payload to JSON
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return defaultPayloadBytes, err
	} else {
		page.display.Debug(fmt.Sprintf(dbgConfluencePageGetNewPayloadEnd, page.Title, page.SpaceKey))
		return payloadBytes, nil
	}
}

// Record the confluence page
func (page *ConfluencePage) GetUpdatePayload(newVersion int) ([]byte, error) {
	page.display.Debug(fmt.Sprintf(dbgConfluencePageGetUpdatePayloadBegin, page.PageID, page.SpaceKey))
	defaultPayloadBytes := []byte{}
	// Create the updated payload
	payload := map[string]interface{}{
		"type": "page",
		"id":   page.PageID,
		"version": map[string]interface{}{
			"number": newVersion,
		},
		"body": map[string]interface{}{
			"storage": map[string]string{
				"value":          page.HtmlContent,
				"representation": "storage",
			},
		},
	}
	// Include  page ID if specified
	if page.Title != "" {
		payload["title"] = page.Title
	}

	// Include parent page if specified
	if page.ParentPageID != "" {
		payload["ancestors"] = []map[string]string{
			{"id": page.ParentPageID},
		}
	}
	// Include parent page if specified
	if page.SpaceKey != "" {
		payload["space"] = map[string]string{
			"key": page.SpaceKey,
		}
	}

	// Convert payload to JSON
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return defaultPayloadBytes, err
	} else {
		page.display.Debug(fmt.Sprintf(dbgConfluencePageGetUpdatePayloadEnd, page.PageID, page.SpaceKey))
		return payloadBytes, nil
	}
}
