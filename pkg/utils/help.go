package utils

import (
	"fmt"
)

// Function used to display answer to the help command (or flag)
func DisplayHelpCmd() {
	display := NewCmdDisplay(GroupName)
	display.Debug("Display the default help message")
	helpMessage := `
sxlibs is an empty command existing just for testing purpose. 

Usage: sxlibs COMMAND [ARGS] [OPTIONS]...

Subcommands:
  version        Get the version of this package

Generic options:
  --debug        Activates debug mode for detailed troubleshooting information.
  --help         Displays this help message and exits.

Examples:

  Return the version of this binary:
    $ sxlibs version

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}
