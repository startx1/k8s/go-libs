package utils

import (
	"fmt"
	"os"
	"time"
)

// The global IsDebug var
var IsDebug bool

// CmdDisplay is a definition for displaying message for a command line
type CmdDisplay struct {
	GroupName string
}

// Initialize a CmdDisplay object
// ex:
//
//	display := NewCmdDisplay("main",true)
func NewCmdDisplay(groupName string) *CmdDisplay {
	return &CmdDisplay{
		GroupName: groupName,
	}
}

//
// Define the trace functions used to display unified trace of the actions
//

// Function used to display a debug trace if IsDebug is on
func (cmdDisplay *CmdDisplay) Debug(msg string) {
	if IsDebug {
		fmt.Printf("%5s [%s] : %-15s : %s\n", "DEBUG", time.Now().Format("15:04:05.000"), cmdDisplay.GroupName, msg)
	}
}

// Function used to display an information
func (cmdDisplay *CmdDisplay) Info(msg string) {
	fmt.Printf("%5s [%s] : %s\n", "INFO", time.Now().Format("15:04:05.000"), msg)
}

// Function used to display an error
func (cmdDisplay *CmdDisplay) Error(msg string) {
	fmt.Printf("%5s [%s] : %s\n", "ERROR", time.Now().Format("15:04:05.000"), msg)
}

// Function used to display an error and return exit
func (cmdDisplay *CmdDisplay) ExitError(msg string, errorCode int) {
	fmt.Printf("%5s : %s\n", "ERROR", msg)
	os.Exit(errorCode)
}

// Function used to display a warning
func (cmdDisplay *CmdDisplay) Warning(msg string) {
	fmt.Printf("%5s [%s] : %s\n", "WARN", time.Now().Format("15:04:05.000"), msg)
}
