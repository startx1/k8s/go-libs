package utils

import (
	"fmt"
	"strings"
)

const (
	// GroupNameArgParser is the group name use in this package
	GroupNameArgParser = "utils.args"
)

// ArgParser is a definition for displaying message for a command line
type ArgParser struct {
	SourceArgs []string
	ParsedArgs []string
	Binary     string
	TopAction  string
	SubAction  string
	IsHelp     bool
	IsDebug    bool
}

// Initialize a ArgParser object
// ex:
//
//	display := NewArgParser("main",true)
func NewArgParser(sourceArgs []string) *ArgParser {
	argParser := &ArgParser{
		SourceArgs: sourceArgs,
		ParsedArgs: sourceArgs,
		Binary:     "",
		TopAction:  "",
		SubAction:  "",
	}
	argParser.scanArgs()
	return argParser
}

const (
	ddDebugAPStart      = "Start display the argParser"
	ddDebugAPBinary     = "   argParser binary       : %s"
	ddDebugAPTopAction  = "   argParser top action   : %s"
	ddDebugAPSubAction  = "   argParser sub action   : %s"
	ddDebugAPIsHelp     = "   argParser has Help     : %t"
	ddDebugAPIsDebug    = "   argParser has Debug    : %t"
	ddDebugAPParsedArgs = "   argParser arguments    : %v"
)

// Display the content of the ArgParser
func (argParser *ArgParser) Debug() *ArgParser {
	display := NewCmdDisplay(GroupNameArgParser)
	display.Debug(ddDebugAPStart)
	display.Debug(fmt.Sprintf(ddDebugAPBinary, argParser.Binary))
	display.Debug(fmt.Sprintf(ddDebugAPTopAction, argParser.TopAction))
	display.Debug(fmt.Sprintf(ddDebugAPSubAction, argParser.SubAction))
	display.Debug(fmt.Sprintf(ddDebugAPIsHelp, argParser.IsHelp))
	display.Debug(fmt.Sprintf(ddDebugAPIsDebug, argParser.IsDebug))
	display.Debug(fmt.Sprintf(ddDebugAPParsedArgs, argParser.ParsedArgs))
	return argParser
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *ArgParser) scanArgs() []string {
	// Parse debug flag
	args := RemoveFlag(argParser.SourceArgs, "--debug")
	args = RemoveFlag(args, "--help")
	if argParser.HasFlag("--debug") {
		argParser.IsDebug = true
		IsDebug = true
	}
	if argParser.HasFlag("--help") {
		argParser.IsHelp = true
	}
	argParser.Binary = args[0]
	if len(args) > 1 {
		argParser.TopAction = args[1]
	}
	if len(args) > 2 {
		argParser.SubAction = args[2]
	}
	argParser.ParsedArgs = args
	return args
}

// used to scan arguments and detect if command has a special flag
func (argParser *ArgParser) HasFlag(flag string) bool {
	for _, arg := range argParser.ParsedArgs {
		if strings.HasPrefix(arg, flag) {
			return true
		}
	}
	return false
}

// used to scan arguments and detect if command has a special flag
func (argParser *ArgParser) GetFlagPos(flag string) int {
	for i, arg := range argParser.ParsedArgs {
		if strings.HasPrefix(arg, flag) {
			return i
		}
	}
	return -1
}

// used to scan arguments and detect if command has a special flag
func (argParser *ArgParser) GetFlagNextVal(flag string) string {
	for i, arg := range argParser.ParsedArgs {
		if strings.HasPrefix(arg, flag) {
			return string(argParser.ParsedArgs[i+1])
		}
	}
	return ""
}

// used to scan arguments and detect if command has a special flag
func (argParser *ArgParser) RemoveFlag(flag string) bool {
	var result []string
	for _, arg := range argParser.ParsedArgs {
		if !strings.HasPrefix(arg, flag) {
			result = append(result, arg)
		}
	}
	argParser.ParsedArgs = result
	return true
}

// used to scan arguments and detect if command has a special flag
func (argParser *ArgParser) RemoveFlagFromPos(pos int) bool {
	var result []string
	for i, arg := range argParser.ParsedArgs {
		if i != pos {
			result = append(result, arg)
		}
	}
	argParser.ParsedArgs = result
	return true
}
