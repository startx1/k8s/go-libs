package utils

import (
	"fmt"
	"os"
	"os/exec"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName = "utils"
)

func RemoveFlag(args []string, flagToRemove string) []string {
	var result []string
	for _, arg := range args {
		if arg != flagToRemove {
			result = append(result, arg)
		}
	}
	return result
}

func RemoveFlagFromPos(args []string, pos int) []string {
	var result []string
	for i, arg := range args {
		if i != pos {
			result = append(result, arg)
		}
	}
	return result
}

// Function used to display the list of quotas
func ExecuteCommand(command string) {
	display := NewCmdDisplay(GroupName)
	display.Debug("Listing content of current namespace")
	cmd := exec.Command("bash", "-c", command)
	output, err := cmd.CombinedOutput()
	if err != nil {
		display.ExitError("Could not execute "+command+" "+err.Error(), 5)
	}

	fmt.Println(string(output))
}

// RQExporterWriteToFile record into a file the given content
func WriteFile(filePath string, content string) error {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		file, err := os.Create(filePath)
		if err != nil {
			return err
		}
		file.Close()
	} else if err != nil {
		return err
	}

	file, err := os.OpenFile(filePath, os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(content)
	if err != nil {
		return err
	}
	return nil
}
