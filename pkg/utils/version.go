package utils

import "fmt"

const (
	versionMajor = "0.2"
	versionMinor = "5"
)

// Function used to display answer to the help command (or flag)
func DisplayVersion() {
	display := NewCmdDisplay(GroupName)
	display.Debug("Display the version message")
	versionMessage := `sxlibs v` + versionMajor + "." + versionMinor
	fmt.Println(versionMessage)
}
