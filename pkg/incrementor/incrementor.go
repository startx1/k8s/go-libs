/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package incrementor

import (
	"fmt"
	"regexp"
	"strconv"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	"k8s.io/apimachinery/pkg/api/resource"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName                 = "incrementor"
	dbgInitIncrNewIncrementor = "Init Incrementor object"
	dbgInitIncrnewIncrementor = "Init a new Incrementor object"
	dbgInitIncrLoadFromString = "Load incrementor value %s"
	dbgIncrStart              = "DEBUG the incrementor"
	dbgIncrValString          = "DEBUG - incrementor string val : %s"
	dbgIncrValInt             = "DEBUG - incrementor string int : %d"
	dbgIncrValQty             = "DEBUG - incrementor string qty : %s"
	dbgIncrValType            = "DEBUG -       incrementor type : %s"
	dbgIncrValUnit            = "DEBUG -       incrementor unit : %s"
	dbgIncrIsPercentage       = "DEBUG - incrementor percentage : %t"
	errIncrLoadParse          = "error parsing %s value : %v"
)

//
// Define object variables
//

// K8sClient represents a wrapper around the Kubernetes clientset.
type Incrementor struct {
	valString    string
	valInt       int
	valQty       resource.Quantity
	valType      string
	valUnit      string
	isPercentage bool
	display      *sxUtils.CmdDisplay
}

// NewIncrementor create a new Incrementor ready to use the clientset
// to interact with the kubernetes cluster
func NewIncrementor(val string) *Incrementor {
	// sxUtils.DisplayDebug(GroupName,dbgInitIncrNewIncrementor)
	incr := newIncrementor()
	incr.LoadFromString(val)
	return incr
}

// newIncrementor create a new Incrementor with default (0) value set
func newIncrementor() *Incrementor {
	// sxUtils.DisplayDebug(GroupName,dbgInitIncrnewIncrementor)
	qty, _ := resource.ParseQuantity("0")
	incr := &Incrementor{
		valString:    "0",
		valInt:       0,
		valQty:       qty,
		valType:      "count",
		valUnit:      "int",
		isPercentage: false,
		display:      sxUtils.NewCmdDisplay(GroupName),
	}
	return incr
}

// NewIncrementor create a new Incrementor ready to use the clientset
// to interact with the kubernetes cluster
func (incr *Incrementor) LoadFromString(val string) *Incrementor {
	incr.display.Debug(fmt.Sprintf(dbgInitIncrLoadFromString, val))
	incr.valString = val
	incr.valType = "count"
	incr.isPercentage = false
	incr.valUnit = ""
	factor := 1
	re := regexp.MustCompile("(-?[0-9]+)([a-zA-Z%]*)$")
	matches := re.FindStringSubmatch(incr.valString)
	if len(matches) > 2 {
		incr.SetValUnit(matches[2])
		incr.valString = matches[1]
		isPercentage := matches[2] == "%"
		if isPercentage {
			incr.isPercentage = true
		} else {
			switch incr.GetValUnit() {
			case "m":
				incr.valType = "cpu"
			case "Pi":
				incr.valType = "mem"
				factor = 1024 * 1024 * 1024 * 1024 * 1024
			case "Ti":
				incr.valType = "mem"
				factor = 1024 * 1024 * 1024 * 1024
			case "Gi":
				incr.valType = "mem"
				factor = 1024 * 1024 * 1024
			case "Mi":
				incr.valType = "mem"
				factor = 1024 * 1024
			case "Ki":
				incr.valType = "mem"
				factor = 1024
			default:
				incr.valType = "count"
			}
		}
	}
	iint, errParsei := strconv.Atoi(incr.valString)
	if errParsei != nil {
		incr.display.ExitError(
			fmt.Sprintf(
				errIncrLoadParse,
				incr.valString,
				errParsei,
			),
			30,
		)
		return nil
	}
	incr.valInt = (iint * factor)
	qty, errParse := resource.ParseQuantity(incr.valString)
	if errParse != nil {
		incr.display.ExitError(
			fmt.Sprintf(errIncrLoadParse, incr.valString, errParse),
			30,
		)
		return nil
	}
	incr.valQty = qty
	return incr
}

// Display the content of the Incrementor
func (incr *Incrementor) Debug() *Incrementor {
	incr.display.Debug(dbgIncrStart)
	incr.display.Debug(fmt.Sprintf(dbgIncrValString, incr.valString))
	incr.display.Debug(fmt.Sprintf(dbgIncrValInt, incr.valInt))
	incr.display.Debug(fmt.Sprintf(dbgIncrValQty, incr.valQty.String()))
	incr.display.Debug(fmt.Sprintf(dbgIncrValType, incr.valType))
	incr.display.Debug(fmt.Sprintf(dbgIncrValUnit, incr.valUnit))
	incr.display.Debug(fmt.Sprintf(dbgIncrIsPercentage, incr.isPercentage))
	return incr
}

// SetIsPercentage sets as a percentage value
func (incr *Incrementor) SetIsPercentage(isPercentage bool) *Incrementor {
	incr.isPercentage = isPercentage
	return incr
}

// IsPercentage returns the incrementor is a percentage.
func (incr *Incrementor) IsPercentage() bool {
	return incr.isPercentage
}

// SetValString sets the valString field.
func (incr *Incrementor) SetValString(val string) *Incrementor {
	incr.LoadFromString(val)
	return incr
}

// GetValString returns the valString field.
func (incr *Incrementor) GetValString() string {
	return incr.valString
}

// SetValInt sets the valInt field.
func (incr *Incrementor) SetValInt(val int) *Incrementor {
	valString := strconv.Itoa(val)
	incr.LoadFromString(valString)
	return incr
}

// GetValInt returns the valInt field.
func (incr *Incrementor) GetValInt() int {
	return incr.valInt
}

// SetValQty sets the valQty field.
func (incr *Incrementor) SetValQty(val resource.Quantity) *Incrementor {
	incr.LoadFromString(val.String())
	return incr
}

// GetValQty returns the valQty field.
func (incr *Incrementor) GetValQty() *resource.Quantity {
	return &incr.valQty
}

// SetValType sets the valType field.
func (incr *Incrementor) SetValType(valType string) *Incrementor {
	incr.valType = valType
	return incr
}

// GetValType returns the valType field.
func (incr *Incrementor) GetValType() string {
	return incr.valType
}

// SetValUnit sets the valUnit field.
func (incr *Incrementor) SetValUnit(valUnit string) *Incrementor {
	incr.valUnit = valUnit
	return incr
}

// GetValUnit returns the valUnit field.
func (incr *Incrementor) GetValUnit() string {
	return incr.valUnit
}

// GetValStringUnit returns the valString + valUnit field.
func (incr *Incrementor) GetValStringUnit() string {
	return fmt.Sprintf("%s%s", incr.GetValString(), incr.GetValUnit())
}
