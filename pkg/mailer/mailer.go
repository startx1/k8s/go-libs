package mailer

import (
	"fmt"
	"io"
	"net/smtp"
	"strconv"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	gomail "gopkg.in/gomail.v2"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName                    = "mailer"
	dbgNewMailer                 = "Initialize a mailer object for %s:%s with username %s"
	dbgMailerSendBegin           = "Begin send from %s to %s subject %s"
	wrnMailerSendUnauthenticated = "using an unauthenticated connection"
	wrnMailerSendInsecure        = "using an insecure connection"
	dbgMailerSendEnd             = "End send from %s to %s subject %s"
	wrnMailerSendNOK             = "Could not send email to %s : %v"
)

// PlainAuth reimplemented without the TLS check if insecure mail is required
// See: https://github.com/golang/go/issues/31825
type unencryptedAuth struct {
	smtp.Auth
}

func (a unencryptedAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	s := *server
	s.TLS = true
	return a.Auth.Start(&s)
}

//
// Define object variables
//

// Mailer is a wrapper around api.Config
type Mailer struct {
	SmtpHost     string
	SmtpPort     string
	SmtpUsername string
	SmtpPassword string
	display      *sxUtils.CmdDisplay
}

//
// Define constructor and public functions
//

// Initialize a mailer object
// ex:
//
//	mailer := kc.NewMailer()
func NewMailer(smtpHost string, smtpPort string, smtpUsername string, smtpPassword string) *Mailer {
	return newMailer(smtpHost, smtpPort, smtpUsername, smtpPassword)
}

// internal used to construct a new api.Config
func newMailer(smtpHost string, smtpPort string, smtpUsername string, smtpPassword string) *Mailer {
	display := sxUtils.NewCmdDisplay(GroupName)
	if smtpHost == "" {
		smtpHost = "smtp.example.com"
	}
	if smtpPort == "" {
		smtpPort = "25"
	}
	display.Debug(fmt.Sprintf(dbgNewMailer, smtpHost, smtpPort, smtpUsername))
	return &Mailer{
		SmtpHost:     smtpHost,
		SmtpPort:     smtpPort,
		SmtpUsername: smtpUsername,
		SmtpPassword: smtpPassword,
		display:      display,
	}
}

// Send an email
func (mailer *Mailer) Send(mail *MailerMail, insecure bool) error {
	mailer.display.Debug(fmt.Sprintf(dbgMailerSendBegin, mail.From, mail.To, mail.Subject))
	smtpPort, _ := strconv.Atoi(mailer.SmtpPort)
	d := gomail.NewDialer(
		mailer.SmtpHost,
		smtpPort,
		mailer.SmtpUsername,
		mailer.SmtpPassword,
	)
	if mailer.SmtpUsername == "" && mailer.SmtpPassword == "" {
		mailer.display.Warning(wrnMailerSendUnauthenticated)
	} else {
		d.Auth = smtp.PlainAuth(
			"",
			mailer.SmtpUsername,
			mailer.SmtpPassword,
			mailer.SmtpHost,
		)
		if insecure {
			mailer.display.Warning(wrnMailerSendInsecure)
			d.Auth = unencryptedAuth{
				smtp.PlainAuth(
					"",
					mailer.SmtpUsername,
					mailer.SmtpPassword,
					mailer.SmtpHost,
				),
			}
		}
	}
	m := gomail.NewMessage()
	m.SetHeader("From", mail.From)
	m.SetHeader("To", mail.To)
	m.SetHeader("Subject", mail.Subject)
	m.SetBody("text/plain", mail.Body)

	for _, attachment := range mail.Attachments {
		m.Attach(attachment.Filename,
			gomail.SetCopyFunc(func(w io.Writer) error {
				_, err := w.Write(attachment.Content)
				return err
			}),
			gomail.SetHeader(map[string][]string{
				"Content-Type":        {fmt.Sprintf("%s; name=\"%s\"", attachment.ContentType, attachment.Filename)},
				"Content-Disposition": {fmt.Sprintf("attachment; filename=\"%s\"", attachment.Filename)},
			}),
		)
	}

	// Send the email
	if errSend := d.DialAndSend(m); errSend != nil {
		mailer.display.Error(fmt.Sprintf(wrnMailerSendNOK, mail.To, errSend))
		return errSend
	} else {
		mailer.display.Debug(fmt.Sprintf(dbgMailerSendEnd, mail.From, mail.To, mail.Subject))
	}
	return nil
}
