package mailer

import (
	"fmt"
	"mime"
	"path/filepath"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

const (
	// GroupName is the group name use in this package
	GroupNameMail          = "mailer.mail"
	dbgNewMailerMail       = "Init MailerMail object with from: %s, to: %s, subject: %s"
	dbgMailerMailSendBegin = "Begin send from %s to %s subject %s"
	dbgMailerMailSendEnd   = "End send from %s to %s subject %s"
)

// Mailer is a wrapper around api.Config
type MailerMail struct {
	From        string
	To          string
	Subject     string
	Body        string
	Attachments []MailerMailAttachment
	display     *sxUtils.CmdDisplay
}
type MailerMailAttachment struct {
	Filename    string
	Content     []byte
	ContentType string
}

//
// Define constructor and public functions
//

// Initialize a mailer object
// ex:
//
//	mailer := kc.NewMailer()
func NewMailerMail(from string, to string, subject string, message string) *MailerMail {
	return newMailerMail(from, to, subject, message)
}

//
// Define object internal methods
//

// internal used to construct a new api.Config
func newMailerMail(from string, to string, subject string, message string) *MailerMail {
	display := sxUtils.NewCmdDisplay(GroupNameMail)
	mail := MailerMail{
		From:    from,
		To:      to,
		Subject: subject,
		Body:    message,
		display: sxUtils.NewCmdDisplay(GroupNameMail),
	}
	mail.
		SetFrom(from).
		SetTo(to).
		SetSubject(subject).
		SetBody(message)

	display.Debug(fmt.Sprintf(dbgNewMailerMail, from, to, subject))
	// sxUtils.DisplayDebug(GroupName,ddInitnewMailer)
	return &mail
}

// Set the from of the mail
func (mail *MailerMail) SetFrom(from string) *MailerMail {
	// sxUtils.DisplayDebug(GroupNameMail, dbgNewMailerMail)
	if from == "" {
		from = "sxcollector@example.com"
	}
	mail.From = from
	return mail
}

// Set the to of the mail
func (mail *MailerMail) SetToSingle(fromString string) *MailerMail {
	// sxUtils.DisplayDebug(GroupNameMail, dbgNewMailerMail)
	from := fromString
	mail.To = from
	return mail
}

// Set the to of the mail
func (mail *MailerMail) SetTo(from string) *MailerMail {
	// sxUtils.DisplayDebug(GroupNameMail, dbgNewMailerMail)
	if len(from) == 0 {
		from = "sxcollector@example.com"
	}
	mail.To = from
	return mail
}

// Set the subject of the mail
func (mail *MailerMail) SetSubject(subject string) *MailerMail {
	// sxUtils.DisplayDebug(GroupNameMail, dbgNewMailerMail)
	if subject == "" {
		subject = "hello from sx-golib"
	}
	mail.Subject = subject
	return mail
}

// Set the body of the mail
func (mail *MailerMail) SetBody(message string) *MailerMail {
	// sxUtils.DisplayDebug(GroupNameMail, dbgNewMailerMail)
	if message == "" {
		message = "Default message from sx-golib"
	}
	mail.Body = message
	return mail
}

// Set the body of the mail
func (mail *MailerMail) AddAttachment(filename string, filecontent []byte) *MailerMail {
	// Determine content type based on file extension
	ext := filepath.Ext(filename)
	contentType := mime.TypeByExtension(ext)
	if contentType == "" {
		contentType = "application/octet-stream"
	}
	mail.Attachments = append(mail.Attachments, MailerMailAttachment{
		Filename:    filename,
		Content:     filecontent,
		ContentType: contentType,
	})
	return mail
}

// Set the body of the mail
func (mail *MailerMail) Send(mailer *Mailer, insecure bool) error {
	mailer.display.Debug(fmt.Sprintf(dbgMailerMailSendBegin, mail.From, mail.To, mail.Subject))
	errSend := mailer.Send(mail, insecure)
	if errSend != nil {
		return errSend
	}
	mailer.display.Debug(fmt.Sprintf(dbgMailerMailSendEnd, mail.From, mail.To, mail.Subject))
	return nil
}
