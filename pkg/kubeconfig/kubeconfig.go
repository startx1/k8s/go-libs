package kubeconfig

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	"gopkg.in/yaml.v2"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName              = "kubeconfig"
	dbgInitnewKubeConfig   = "Init Kubeconfig object"
	dbgNewKubeConfig       = "Initialize a kubeconfig object"
	dbgStartLoadFromFile   = "Load kubeconfig from file %s"
	dbgStartLoadFromEnv    = "Load kubeconfig from the KUBECONFIG env var"
	dbgStartLoadFromHome   = "Load kubeconfig from the home directory"
	dbgStartLoadFromYaml   = "Load kubeconfig from a Yaml object"
	dbgStartLoadFromString = "Load kubeconfig from a YAML string"
	dbgStartLoad           = "Automatic search of an available kubeconfig"
	dbgStartSave           = "save the kubeconfig to the path"
	dbgStartConnect        = "Connect to the cluster"
	dbgStartDisconnect     = "Disconnect from the cluster"
	dbgKCfgStart           = "DEBUG the kubeconfig"
	dbgKCfgConfig          = "DEBUG - kubeconfig string val     : %s"
	dbgKCfgPath            = "DEBUG - kubeconfigpath string val : %s"
)

//
// Define object variables
//

// Kubeconfig is a wrapper around api.Config
type Kubeconfig struct {
	Config  string
	Path    string
	display *sxUtils.CmdDisplay
}

//
// Define constructor and public functions
//

// Initialize a kubeconfig object
// ex:
//
//	kubeconfig := kc.NewKubeConfig()
func NewKubeConfig() *Kubeconfig {
	// sxUtils.DisplayDebug(GroupName,dbgNewKubeConfig)
	return newKubeConfig()
}

//
// Define object internal methods
//

// internal used to construct a new api.Config
func newKubeConfig() *Kubeconfig {
	// sxUtils.DisplayDebug(GroupName,dbgInitnewKubeConfig)
	return &Kubeconfig{
		Config:  "",
		Path:    "",
		display: sxUtils.NewCmdDisplay(GroupName),
	}
}

//
// Define object external methods
//

// Loading Kubeconfig from a file
// ex:
//
//	err := kc.LoadFromFile("/my/path/kubeconfig")
func (kc *Kubeconfig) LoadFromFile(filePath string) (*Kubeconfig, error) {
	kc.display.Debug(fmt.Sprintf(dbgStartLoadFromFile, filePath))
	yamlData, err := os.ReadFile(filePath)
	if err != nil {
		return kc, err
	}
	kc.Config = string(yamlData)
	kc.Path = filePath
	return kc, nil
}

// Loading from the KUBECONFIG env var
// ex:
//
//	err := kc.LoadFromEnv()
func (kc *Kubeconfig) LoadFromEnv(fatal bool) (*Kubeconfig, error) {
	kc.display.Debug(dbgStartLoadFromEnv)
	kubeconfigPath := os.Getenv("KUBECONFIG")
	if kubeconfigPath == "" {
		if fatal {
			kc.display.ExitError("Could not find the KUBECONFIG environment variable", 30)
		} else {
			return kc, errors.New("could not find the KUBECONFIG environment variable")
		}
	} else {
		return kc.LoadFromFile(kubeconfigPath)
	}
	return kc, errors.New("could not find the KUBECONFIG environment variable")
}

// Return a display of the current object
// ex:
//
//	kc.Debug()
func (kc *Kubeconfig) Debug() *Kubeconfig {
	kc.display.Debug(dbgKCfgStart)
	kc.display.Debug(fmt.Sprintf(dbgKCfgConfig, kc.GetConfig()))
	kc.display.Debug(fmt.Sprintf(dbgKCfgPath, kc.GetPath()))
	return kc
}

// Loading kubeconfig from the ~/.kube/config
// ex:
//
//	err := kc.LoadFromHome()
func (kc *Kubeconfig) LoadFromHome() (*Kubeconfig, error) {
	kc.display.Debug(dbgStartLoadFromHome)
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return kc, err
	}
	kubeconfigPath := filepath.Join(homeDir, ".kube", "config")
	return kc.LoadFromFile(kubeconfigPath)
}

// Loading kubeconfig from YAML content
// ex:
//
//	yamlData := []byte(`your YAML data here`)
//	err := kc.LoadFromYaml(yamlData)
func (kc *Kubeconfig) LoadFromYaml(yamlData []byte) *Kubeconfig {
	kc.display.Debug(dbgStartLoadFromYaml)
	kc.Config = string(yamlData)
	kc.Path = "memory"
	return kc
}

// Loading kubeconfig from a YAML string
// ex:
//
//	yamlString := `your YAML string here`
//	err := kc.LoadFromString(yamlString)
func (kc *Kubeconfig) LoadFromString(yamlString string) *Kubeconfig {
	kc.display.Debug(dbgStartLoadFromString)
	return kc.LoadFromYaml([]byte(yamlString))
}

// Automatic search of an available kubeconfig
// ex:
//
//	kc.Load()
func (kc *Kubeconfig) Load(exit bool) error {
	kc.display.Debug(dbgStartLoad)
	kc.LoadFromEnv(false)
	_, errLoadHome := kc.LoadFromHome()
	if errLoadHome != nil && exit {
		kc.display.ExitError(errLoadHome.Error(), 20)
	}
	return errLoadHome
}

// Automatic search of an available kubeconfig
// ex:
//
//	kc.Load()
func (kc *Kubeconfig) Save(exit bool) error {
	kc.display.Debug(dbgStartSave)
	_, errSave := kc.LoadFromHome()
	if errSave != nil {
		kc.display.ExitError(errSave.Error(), 20)
	}
	return errSave
}

// Connect to the cluster
// ex:
//
//	kc.Connect()
func (kc *Kubeconfig) Connect(exit bool) error {
	kc.display.Debug(dbgStartConnect)
	// TODO: Connect to the cluster
	return nil
}

// Disconnect from the cluster
// ex:
//
//	kc.Connect()
func (kc *Kubeconfig) Disconnect(exit bool) error {
	kc.display.Debug(dbgStartDisconnect)
	// TODO: Connect to the cluster
	return nil
}

// Get the kubeconfig string (alias to GetConfig)
// ex:
//
//	kc := NewKubeConfig().Get()
func (kc *Kubeconfig) Get() string {
	return kc.GetConfig()
}

// Get the kubeconfig string (alias to GetConfigString)
// ex:
//
//	kc := NewKubeConfig().GetConfig()
func (kc *Kubeconfig) GetConfig() string {
	return kc.GetConfigString()
}

// Get the kubeconfig string
// ex:
//
//	kc := NewKubeConfig().GetConfigString()
func (kc *Kubeconfig) GetConfigString() string {
	return kc.Config
}

// Get the kubeconfig filepath
// ex:
//
//	kc := NewKubeConfig().GetPath()
func (kc *Kubeconfig) GetPath() string {
	return kc.Path
}

// Get the kubeconfig content returned in bytes
// ex:
//
// kc := NewKubeConfig().GetConfigBytes()
func (kc *Kubeconfig) GetConfigBytes() ([]byte, error) {
	yamlData, err := yaml.Marshal(kc.Config)
	if err != nil {
		return nil, err
	}
	return yamlData, nil
}

// Get the kubeconfig content returned in yaml
// ex:
//
// kc := NewKubeConfig().GetYaml()
func (kc *Kubeconfig) GetYaml() string {
	return kc.Config
}
