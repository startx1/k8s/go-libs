package k8sclient

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strings"

	ocpcs "github.com/openshift/client-go/config/clientset/versioned"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxlibs.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName                           = "k8sclient"
	dbgInitnewK8sClient                 = "Create a new Kubernetes client"
	dbgGetCurrentNamespace              = "Return the current namespace : %s"
	errBuildClient                      = "error building k8sclient : %v"
	errBuildClientset                   = "error creating Kubernetes clientset : %v"
	errConnectingCluster                = "error connecting to cluster : %v"
	errGetNamespace                     = "error getting he namespace : %v"
	wrnGetNamespace                     = "error retrieving namespace from kubeconfig %s : %v. using default"
	dbgKCliStart                        = "DEBUG the k8sclient"
	dbgKCliConfig                       = "DEBUG -     server URL : %s"
	dbgKCliPath                         = "DEBUG - kubeconfigpath : %s"
	dbgInitGetCurrentNamespace          = "Init getting the current namespace"
	dbgInitListAllNamespaces            = "Init listing all namespaces"
	dbgInitListAllMatchingNamespaces    = "Init listing all namespaces matching %s pattern"
	dbgInitNewK8sClient                 = "k8sclient : Initialize a Kubernetes client"
	dbgInitNewOcpClient                 = "k8sclient : Initialize a Openshift client"
	dbgListAllMatchingNamespacesFilter  = "Filtering namespaces matching %s pattern"
	dbgListAllMatchingNamespacesCounter = "Found %d namespaces matching %s pattern"
	dbgListAllNamespacesCounter         = "Found %d namespaces in this cluster"
	errGetKubeconfigTokenFileNOK        = "failed to read bearer token from file %s : %v"
	errGetKubeconfigTokenNOK            = "failed to find a bearer or file token for the current kubeconfig context"
	dbgGetKubeconfigTokenOK             = "found kubeconfig bearer token %s"
)

//
// Define object variables
//

// K8sClient represents a wrapper around the Kubernetes clientset.
type K8sClient struct {
	*kubernetes.Clientset
	config         *rest.Config
	kubeconfigpath string
	display        *sxUtils.CmdDisplay
}

//
// Define constructor and public functions
//

// Initialize a K8sClient object
// ex:
//
//	k8sclient := kc.NewK8sClient()
func NewK8sClient(kubeconfigpath string, isOpenshift bool, forceInCluster bool) *K8sClient {
	display := sxUtils.NewCmdDisplay(GroupName)
	msg := dbgInitNewK8sClient
	if isOpenshift {
		msg = dbgInitNewOcpClient
	}
	display.Debug(msg)
	client, err := newK8sclient(kubeconfigpath, forceInCluster)
	if err != nil {
		display.ExitError(fmt.Sprintf(errConnectingCluster, err), 30)
	}
	return client
}

//
// Define object internal methods
//

// newK8sclient creates a new instance of K8sClient.
func newK8sclient(kubeconfigpath string, forceInCluster bool) (*K8sClient, error) {
	// sxUtils.DisplayDebug(GroupName,dbgInitnewK8sClient)
	// Get Kubernetes client config
	config, err := rest.InClusterConfig()
	if forceInCluster && err != nil {
		return nil, fmt.Errorf(errBuildClient, err)
	} else if err != nil {
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfigpath)
		if err != nil {
			return nil, fmt.Errorf(errBuildClient, err)
		}
	}
	clientsetK8S, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf(errBuildClientset, err)
	}
	return &K8sClient{
		Clientset:      clientsetK8S,
		kubeconfigpath: kubeconfigpath,
		config:         config,
		display:        sxUtils.NewCmdDisplay(GroupName),
	}, nil
}

// newOcpClientSet creates a new openshift clientset.
func (kc *K8sClient) NewOCPClientSet() (*ocpcs.Clientset, error) {
	// sxUtils.DisplayDebug(GroupName,dbgInitnewK8sClient)
	// Get Openshift clientset
	clientsetOCP, err := ocpcs.NewForConfig(kc.config)
	if err != nil {
		return nil, fmt.Errorf(errBuildClientset, err)
	}
	return clientsetOCP, nil
}

//
// Define object external methods
//

// Return a display of the current object
// ex:
//
//	kc.Debug()
func (kc *K8sClient) Debug() *K8sClient {
	kc.display.Debug(dbgKCliStart)
	kc.display.Debug(fmt.Sprintf(dbgKCliConfig, kc.Clientset.RESTClient().Get().URL()))
	kc.display.Debug(fmt.Sprintf(dbgKCliPath, kc.kubeconfigpath))
	return kc
}

// Return the config resource
// ex:
//
//	kc.Debug()
func (kc *K8sClient) GetConfig() *rest.Config {
	return kc.config
}

// ConnectToCluster establishes a connection to the Kubernetes cluster.
func (kc *K8sClient) IsConnected() bool {
	// Perform a simple operation to check the connectivity
	_, err := kc.CoreV1().Namespaces().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return false
	} else {
		return true
	}
}

// Return the name of the kubeconfig path
// ex:
//
//	kubeconfigpath := kc.GetKubeconfigPath()
func (kc *K8sClient) GetKubeconfigPath() string {
	return string(kc.kubeconfigpath)
}

// Return the name of the current namespace
// ex:
//
//	namespace := kc.GetCurrentNamespace()
func (kc *K8sClient) GetCurrentNamespace() string {
	kc.display.Debug(dbgInitGetCurrentNamespace)
	namespace, _, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: kc.kubeconfigpath},
		&clientcmd.ConfigOverrides{}).Namespace()
	if err != nil {
		kc.display.Warning(fmt.Sprintf(wrnGetNamespace, kc.kubeconfigpath, err))
		return "default"
	}
	kc.display.Debug(fmt.Sprintf(dbgGetCurrentNamespace, namespace))
	return namespace
}

// Return the list of all the namespaces
// ex:
//
//	namespaces := kc.ListAllNamespaces()
func (kc *K8sClient) ListAllNamespaces() ([]string, error) {
	kc.display.Debug(dbgInitListAllNamespaces)
	var matchedNamespaces []string
	namespaces, err := kc.Clientset.CoreV1().Namespaces().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	for _, namespace := range namespaces.Items {
		matchedNamespaces = append(matchedNamespaces, namespace.Name)
	}
	kc.display.Debug(fmt.Sprintf(dbgListAllNamespacesCounter, len(matchedNamespaces)))

	return matchedNamespaces, nil
}

// Return the list of all the namespaces matching the pattern
// ex:
//
//	namespaces := kc.ListAllMatchingNamespaces("*dev*")
func (kc *K8sClient) ListAllMatchingNamespaces(pattern string) ([]string, error) {
	kc.display.Debug(fmt.Sprintf(dbgInitListAllMatchingNamespaces, pattern))
	// Replace all '*' with '.*' to simulate a more 'glob-like' pattern
	adjustedPattern := "^" + strings.ReplaceAll(pattern, "*", ".*") + "$"
	re, errPattern := regexp.Compile(adjustedPattern)
	if errPattern != nil {
		return nil, errPattern
	}

	namespaces, errListNS := kc.ListAllNamespaces()
	if errListNS != nil {
		return nil, errListNS
	}

	var matchedNamespaces []string
	kc.display.Debug(fmt.Sprintf(dbgListAllMatchingNamespacesFilter, pattern))
	for _, namespace := range namespaces {
		if re.MatchString(namespace) {
			matchedNamespaces = append(matchedNamespaces, namespace)
		}
	}
	kc.display.Debug(fmt.Sprintf(dbgListAllMatchingNamespacesCounter, len(matchedNamespaces), pattern))
	return matchedNamespaces, nil
}

// Get the token from the kubeconfig used
func (kc *K8sClient) GetKubeconfigToken() (string, error) {
	display := sxUtils.NewCmdDisplay(GroupName)
	token := ""
	// Retrieve the token from the rest.Config
	kconfig := kc.GetConfig()
	if kconfig.BearerToken != "" {
		token = kconfig.BearerToken
	} else if kconfig.BearerTokenFile != "" {
		tokenB, err := os.ReadFile(kconfig.BearerTokenFile)
		if err != nil {
			display.Error(fmt.Sprintf(errGetKubeconfigTokenFileNOK, kconfig.BearerTokenFile, err))
		}
		token = string(tokenB)
	} else {
		display.Error(errGetKubeconfigTokenNOK)
	}
	display.Debug(fmt.Sprintf(dbgGetKubeconfigTokenOK, token))
	return token, nil
}
