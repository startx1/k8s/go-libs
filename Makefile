.PHONY: all clean test

# Name of the Go source file
SRC = main.go

# Name of the binary output
BIN = sxlibs

all: build

# Build the binary
build:
	go get -v -d ./...
	go mod tidy
	mkdir -p bin/
	CGO_ENABLED=0 
	GOOS=linux 
	GOARCH=amd64
	go build -tags 'osusergo netgo' -ldflags "-s -w -extldflags -static" -o bin/$(BIN) $(SRC)
	file bin/$(BIN)
	ls -la bin/$(BIN)

update:
	go get -u ./...

# Release the binary
release: build test

# Run tests
test:
	echo "Running tests..."
	go test -v ./... -coverprofile .testCoverage.txt
	cat .testCoverage.txt

# Clean generated files
clean:
	rm -f $(BIN)